
var map_events = null;
var markers = [];
var markerclusterer = null;


var styles = [{
    url: '/pics/markerclusterer/m1.png',
    height: 52,
    width: 53
},{
    url: '/pics/markerclusterer/m2.png',
    height: 55,
    width: 56
},{
    url: '/pics/markerclusterer/m3.png',
    height: 65,
    width: 66
},{
    url: '/pics/markerclusterer/m4.png',
    height: 77,
    width: 78
},{
    url: '/pics/markerclusterer/m5.png',
    height: 89,
    width: 90
}];

$(document).ready(function() {
    var opt = {
       
        center: new google.maps.LatLng(38.02670629333614, -97.009765625),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: 2,
        mapTypeControl: false
    }
    map_events = new google.maps.Map(document.getElementById("map"), opt);
      var infowindow = new google.maps.InfoWindow({
        maxWidth: 240
       
    });
    var bounds = new google.maps.LatLngBounds();

    for(i in points) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(points[i][0], points[i][1]),
            map: map_events,
            title: points[i][3]
        });
        marker.content = points[i][2];

        bounds.extend(marker.getPosition());

        google.maps.event.addListener(marker, 'click', function() {
            var marker = this;
            infowindow.events = marker.content;
            infowindow.event_index = 0;
            if(marker.content.length == 1) {
                infowindow.setContent(infowindow.events[infowindow.event_index]);
            } else {
                var pagination = "Event 1 of " + infowindow.events.length + " <a href=\"#\" id=\"next_event\">&raquo;&nbsp;</a>";
                infowindow.setContent("<div class=\"map_listing_pagination\">" + pagination + "</div>" + infowindow.events[infowindow.event_index]);
            }
            infowindow.open(map_events, marker);
        });

        markers.push(marker);
    }

    if(points.length) {
        var zoomListener = google.maps.event.addListener(map_events, 'zoom_changed', function() {
            google.maps.event.removeListener(zoomListener);
            zoom = 1; // mike change the setting 
            if(zoom > 2) {
                zoom -= 1;
            }
            if(zoom > 12) {
                zoom = 12;
            }
            this.setZoom(zoom);
        });
        map_events.fitBounds(bounds);
    }

    markerclusterer = new MarkerClusterer(map_events, markers, {gridSize: 40, maxZoom: 8, styles: styles});
	markerclusterer.setCalculator(function(markers, numStyles) {
		var index = 0;
		var count = 0;
		for(i in markers) {
			count += markers[i].content.length;
		}
		var dv = count;
		while (dv !== 0) {
			dv = parseInt(dv / 10, 10);
			index++;
		}

		index = Math.min(index, numStyles);
		return {
			text: count,
			index: index
		};
	});
    
    $("#next_event").live("click", function() {
        var event_index = ++infowindow.event_index % infowindow.events.length;
        var pagination = "Event " + (event_index+1) + " of " + infowindow.events.length;
        if(event_index > 0) {
            pagination = "<a href=\"#\" id=\"previous_event\">&nbsp;&laquo;</a> " + pagination;
        }
        if(event_index < infowindow.events.length - 1) {
            pagination = pagination + " <a href=\"#\" id=\"next_event\">&raquo;&nbsp;</a>";
        }
        infowindow.setContent("<div class=\"map_listing_pagination\">" + pagination + "</div>" + infowindow.events[event_index]);
        return false;
    });
    
    $("#previous_event").live("click", function() {
        var event_index = --infowindow.event_index % infowindow.events.length;
        var pagination = "Event " + (event_index+1) + " of " + infowindow.events.length;
        if(event_index > 0) {
            pagination = "<a href=\"#\" id=\"previous_event\">&nbsp;&laquo;</a> " + pagination;
        }
        if(event_index < infowindow.events.length - 1) {
            pagination = pagination + " <a href=\"#\" id=\"next_event\">&raquo;&nbsp;</a>";
        }
        infowindow.setContent("<div class=\"map_listing_pagination\">" + pagination + "</div>" + infowindow.events[event_index]);
        return false;
    });
});