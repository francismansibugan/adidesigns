

$(document).ready(function() {
var map = null;
var markers = [];
var markerclusterer = null;

var styles = [{
    url: '/pics/markerclusterer/m1.png',
    height: 52,
    width: 53
},{
    url: '/pics/markerclusterer/m2.png',
    height: 55,
    width: 56
},{
    url: '/pics/markerclusterer/m3.png',
    height: 65,
    width: 66
},{
    url: '/pics/markerclusterer/m4.png',
    height: 77,
    width: 78
},{
    url: '/pics/markerclusterer/m5.png',
    height: 89,
    width: 90
}];
    var opt = {
        zoom: 4,
        center: new google.maps.LatLng(38.02670629333614, -97.009765625),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    }
    map = new google.maps.Map(document.getElementById("map2"), opt);
    var infowindow = new google.maps.InfoWindow({
        maxWidth: 240
    });
    var bounds = new google.maps.LatLngBounds();

    for(i in points_jobs) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(points_jobs[i][0], points_jobs[i][1]),
            map: map,
            title: points_jobs[i][3]
        });
        marker.content  = "<h6>Jobs at <a href=\"/jobs/company/"+points_jobs[i][5]+"\">"+points_jobs[i][2]+"</a>:</h6>";
		marker.content += "<p style=\"font-size: .8em\">";
		
		for(j in points_jobs[i][4]) {
			marker.content += "<a href=\"/jobs/"+points_jobs[i][4][j][1]+"\" target=\"_blank\">"+points_jobs[i][4][j][0]+"</a><br />";
		}

		marker.content += "</p>";

		marker.num_jobs = points_jobs[i][4].length;

        bounds.extend(marker.getPosition());

        google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent(this.content);
            infowindow.open(map, this);
        });

        markers.push(marker);
    }

    if(markers.length) {
        var zoomListener = google.maps.event.addListener(map, 'zoom_changed', function() {
            google.maps.event.removeListener(zoomListener);
            //zoom = this.getZoom();
            zoom = 1;
            if(zoom > 2) {
                zoom -= 1;
            }
            if(zoom > 12) {
                zoom = 12;
            }
            this.setZoom(zoom);
        });
        map.fitBounds(bounds);
    }

    markerclusterer = new MarkerClusterer(map, markers, {gridSize: 40, maxZoom: 8, styles: styles});
	markerclusterer.setCalculator(function(markers, numStyles) {
		var index = 0;
		var count = 0;
		for(i in markers) {
			count += markers[i].num_jobs;
		}
		var dv = count;
		while (dv !== 0) {
			dv = parseInt(dv / 10, 10);
			index++;
		}

		index = Math.min(index, numStyles);
		return {
			text: count,
			index: index
		};
	});
});