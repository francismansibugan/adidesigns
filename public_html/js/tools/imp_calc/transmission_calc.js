function asym_stripline()
{
var t = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var h1 = parseFloat(document.getElementById("sub_height1").value)*document.getElementById("sub_height1_unit").value;//substrate height
var h2 = parseFloat(document.getElementById("sub_height2").value)*document.getElementById("sub_height2_unit").value;//substrate height
var w = parseFloat(document.getElementById("width").value)*document.getElementById("width_unit").value;//trace width
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=asym_stripline_calc(w,t,h1,h2,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms"
}

function asym_stripline_calc(w,t,h1,h2,er)
{

var h_eff = (h1+h2)/2;
var m = (6*h_eff)/(3*h_eff+t);

var zo_ss_h_eff = sym_stripline(w,t,h_eff,1);

var zo_ss_h1 = sym_stripline(w,t,h1,1);

var zo_ss_h2 = sym_stripline(w,t,h2,1);

var zo_air = 2*((zo_ss_h1.zo*zo_ss_h2.zo)/(zo_ss_h1.zo+zo_ss_h2.zo));
var delta_zo_air = .0325*Math.PI*Math.pow(zo_air,2)*Math.pow(Math.abs(.5-.5*((2*h1+t)/(h1+h2+t))),2.2)*Math.pow(Math.abs((t+w)/(h1+h2+t)),2.9);
var zo_as = (1/Math.sqrt(er))*(zo_ss_h_eff.zo-delta_zo_air);
return{zo:zo_as};
}

function sym_stripline(w,t,h,er)
{

m = (6*h)/(3*h+t);
w_eff = w+(t/Math.PI)*Math.log(Math.exp(1)/Math.sqrt(Math.pow((t/(4*h+t)),2)+Math.pow(((Math.PI*t)/(4*(w+1.1*t))),m)));
no = 377;
zo_ss = (no/(2*Math.PI*Math.sqrt(er)))*Math.log(1+((8*h)/(Math.PI*w_eff))*(((16*h)/(Math.PI*w_eff))+Math.sqrt(Math.pow(((16*h)/(Math.PI*w_eff)),2)+6.27)));
b = 2*h+t;
D = (w/2)*(1+(t/(Math.PI*w))*(1+Math.log((4*Math.PI*w)/t))+.551*Math.pow((t/w),2));
zo_ss_t2 = (60/Math.sqrt(er))*Math.log((4*b)/(Math.PI*D));
theta = ((2*b)/(b-t))*Math.log((2*b-t)/(b-t))-(t/(b-t))*Math.log((2*b*t-Math.pow(t,2))/(Math.pow((b-t),2)));
zo_ss_w2 = 94.15/(((w/b)/(1-(t/b)))+(theta/Math.PI));
if (((w/b)<.35) ||((t/b) <= .25)||((t/w) <=.11)){
	zo = zo_ss_t2;}
else{
	zo = zo_ss_w2;}

return {zo:zo};
}




function bc_stripline()
{
var t = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var b = parseFloat(document.getElementById("sub_height").value)*document.getElementById("sub_height_unit").value;//substrate height
var w = parseFloat(document.getElementById("width").value)*document.getElementById("width_unit").value;//trace thickness
var s = parseFloat(document.getElementById("space").value)*document.getElementById("space_unit").value;//substrate height
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=bc_stripline_calc(w,t,s,b,er)
document.getElementById("zo_odd_value").innerHTML=trace.zo_odd.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_even_value").innerHTML=trace.zo_even.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_common_value").innerHTML=trace.zo_common.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_diff_value").innerHTML=trace.zo_diff.toPrecision(3)+"&nbsp;ohms";          
}
function bc_stripline_calc(w,t,s,b,er)
{
var k = findk(w,b,s);
var zo_odd=BCS_o(w,b,s,er,k);
var zo_even=BCS_e(w,b,s,er,k);
var zo_common = zo_even/2;
var zo_diff = zo_odd*2;
return{zo_odd:zo_odd,zo_even:zo_even,zo_common:zo_common,zo_diff:zo_diff};
}

function findk(w,b,s)
{

k=.005;
min_error = 100;
real_k = 100;
loops = 1000;
step = 1/loops;
for (n=1; n<=loops; n=n+1)
{
    R=Math.sqrt((((k*b)/s)-1)/((b/(k*s))-1));
    w_b = w/b;
    expression = (1/Math.PI)*(Math.log((1+R)/(1-R))-(s/b)*Math.log((1+R/k)/(1-(R/k))));
    error = Math.abs(w_b-expression);
    if (error < min_error){
        real_k = k;
        min_error = error;
        }
    k=k+step;
}

return real_k
}


    function bangbang(x)
    {
		var result, i, x2;

		result = 1;
		if (x == 0 || x == (-1))
			return(result);

		x2 = x/2;
		for (i = 0; i < x2; i++)
		{
			result *= x;
			x -= 2;
		}

		return result;
    }


function eliptical(k)
{
temp =1
for (n=1; n<=20; n=n+1)
	{
	a = 2*n-1;
	b = 2*n;
	fa = bangbang(a);
	fb = bangbang(b);
	temp = temp+Math.pow((fa/fb),2)*Math.pow(k,b);
	}
result = Math.PI*temp/2;
return(result);
}


function inv_tanh(x)
{
ans=.5*(Math.log((1+x)/(1-x)));
return ans;
}


function BCS_o(w,b,s,er,k)
{
 ZO_o=293.9/(Math.sqrt(er))*(s/b)*(1/inv_tanh(k));
return ZO_o;
}

function BCS_e(w,b,s,er,k)
{
k_p=Math.sqrt(1-Math.pow(k,2));
 ZO_e=(377/Math.sqrt(er))*(eliptical(k_p)/eliptical(k));
return ZO_e;
}




function coax()
{
var d1 = parseFloat(document.getElementById("d1").value)*document.getElementById("d1_unit").value;//
var d2 = parseFloat(document.getElementById("d2").value)*document.getElementById("d2_unit").value;//
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=coax_calc(d1,d2,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms"
document.getElementById("delay_value").innerHTML=trace.delay.toPrecision(3)+"&nbsp;ns/in"
document.getElementById("capacitance_value").innerHTML=trace.cap.toPrecision(3)+"&nbsp;pF/in"
document.getElementById("inductance_value").innerHTML=trace.ind.toPrecision(3)+"&nbsp;nH/in"
}
function coax_calc(d1,d2,er)
{
var zo = (60/Math.sqrt(er))*Math.log(d2/d1);
var delay=(84.72e-12*Math.sqrt(er))/1e-9;
var ind=(5.08e-9*Math.log(d2/d1))/1e-9;
var cap=((1.41e-12/Math.log(d2/d1))*er)/1e-12;
return{zo:zo,delay:delay,ind:ind,cap:cap};
}




function ec_microstrip()
{
var t = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var h = parseFloat(document.getElementById("sub_height").value)*document.getElementById("sub_height_unit").value;//substrate height
var w = parseFloat(document.getElementById("width").value)*document.getElementById("width_unit").value;//trace thickness
var s = parseFloat(document.getElementById("space").value)*document.getElementById("space_unit").value;//substrate height
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=ec_microstrip_calc(w,t,h,s,er);
document.getElementById("zo_odd_value").innerHTML=trace.zo_odd.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_even_value").innerHTML=trace.zo_even.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_common_value").innerHTML=trace.zo_common.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_diff_value").innerHTML=trace.zo_diff.toPrecision(3)+"&nbsp;ohms";  
}

function ec_microstrip_calc(w,t,h,s,er)
{
var no = 377;
var er_eff1 = ((er+1)/2)+((er-1)/2)*(Math.sqrt(w/(w+12*h))+.04*Math.pow((1-(w/h)),2));
var er_eff2 = ((er+1)/2)+((er-1)/2)*(Math.sqrt(w/(w+12*h)));
var er_eff=0;
if((w/h)<1){
	er_eff = er_eff1;}
else{
	er_eff = er_eff2;}
var u=w/h;
var a0 = .7287*(er_eff-.5*(er+1))*(1-Math.exp(-.179*u));
var b0 = (.747*er)/(.15+er);
var c0 = b0-(b0-.207)*Math.exp(-.414*u);
var d0 = .593+.694*Math.exp(-.562*u);
var g = s/h;
var w_eff = w+(t/Math.PI)*Math.log((4*Math.exp(1))/Math.sqrt(Math.pow((t/h),2)+Math.pow((t/(w*Math.PI+1.1*t*Math.PI)),2)))*((er_eff+1)/(2*er_eff));
var er_eff_o = (.5*(er+1)+a0-er_eff)*Math.exp(-c0*Math.pow(g,d0))+er_eff;
var zo_surf = (no/(2*Math.PI*Math.sqrt(2)*Math.sqrt(er_eff+1)))*Math.log(1+4*(h/w_eff)*(4*(h/w_eff)*((14*er_eff+8)/(11*er_eff))+Math.sqrt(16*Math.pow((h/w_eff),2)*Math.pow(((14*er_eff+8)/(11*er_eff)),2)+((er_eff+1)/(2*er_eff))*Math.pow(Math.PI,2))));
var q1 = .8695*Math.pow(u,.194);
var q2 = 1+.7519*g+.189*Math.pow(g,2.31);
var q3 = .1975+Math.pow((16.6+Math.pow((8.4/g),6)),-.387)+(1/241)*Math.log((Math.pow(g,10))/(1+Math.pow((g/3.4),10)));
var q4 = 2*q1/(q2*(Math.exp(-g)*Math.pow(u,q3)+(2-Math.exp(-g))*Math.pow(u,-q3)));
var q5 = 1.794+1.14*Math.log(1+(.638/(g+.517*Math.pow(g,2.43))));
var q6 = .2305+(1/281.3)*Math.log(Math.pow(g,10)/(1+Math.pow((g/5.8),10)))+(1/5.1)*Math.log(1+.598*Math.pow(g,1.154));
var q7 = (10+190*Math.pow(g,2))/(1+82.3*Math.pow(g,3));
var q8 = Math.exp(-6.5-.95*Math.log(g)-Math.pow((g/.15),5));
var q9 = Math.log(q7)*(q8+(1/16.5));
var q10 = (1/q2)*(q2*q4-q5*Math.exp(Math.log(u)*q6*Math.pow(u,-q9)));
var zo_odd = (zo_surf*Math.sqrt(er_eff/er_eff_o))/(1-(zo_surf/no)*q10*Math.sqrt(er_eff));
var v = ((u*(20+Math.pow(g,2)))/(10+Math.pow(g,2)))+g*Math.exp(-g);
var aev=1+((Math.log((Math.pow(v,4)+Math.pow((v/52),2))/(Math.pow(v,4)+.432)))/49)+((Math.log(1+Math.pow((v/18.1),3)))/18.7);
var beer = .564*Math.pow(((er-.9)/(er+3)),.053);
var er_eff_e = .5*(er+1)+.5*(er-1)*(1+Math.pow((10/v),(-aev*beer)));
var zo_even = (zo_surf*Math.sqrt(er_eff/er_eff_e))/(1-(zo_surf/no)*q4*Math.sqrt(er_eff));
var zo_common = zo_even/2;
var zo_diff = zo_odd*2;


return{zo_odd:zo_odd,zo_even:zo_even,zo_common:zo_common,zo_diff:zo_diff};
}




function ec_stripline()
{
var t = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var b = parseFloat(document.getElementById("sub_height").value)*document.getElementById("sub_height_unit").value;//substrate height
var w = parseFloat(document.getElementById("width").value)*document.getElementById("width_unit").value;//trace thickness
var s = parseFloat(document.getElementById("space").value)*document.getElementById("space_unit").value;//substrate height
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=ecs_stripline_calc(w,t,s,b,er)
document.getElementById("zo_odd_value").innerHTML=trace.zo_odd.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_even_value").innerHTML=trace.zo_even.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_common_value").innerHTML=trace.zo_common.toPrecision(3)+"&nbsp;ohms";
document.getElementById("zo_diff_value").innerHTML=trace.zo_diff.toPrecision(3)+"&nbsp;ohms";  
}

function bangbang(x)
{
var result, i, x2;

result = 1;
if (x == 0 || x == (-1))
	return(result);

x2 = x/2;
for (i = 0; i < x2; i++)
{
	result *= x;
	x -= 2;
}

return result;
}


function eliptical(k)
{
var temp =1
var n=0;
for (n=1; n<=20; n=n+1)
	{
var a = 2*n-1;
var b = 2*n;
var fa = bangbang(a);
var fb = bangbang(b);
var temp = temp+Math.pow((fa/fb),2)*Math.pow(k,b);
	}
var result = Math.PI*temp/2;
return(result);
}

function tanh(x)
{
var result = ((Math.exp(2*x)-1)/(Math.exp(2*x)+1));
return (result);
}

function coth(x)
{
var result = ((Math.exp(2*x)+1)/(Math.exp(2*x)-1));
return(result);
}

function sech(x)
{
var result = 2/(Math.exp(x)+Math.exp(-x));
return(result);
}

function sym_stripline(w,t,h,er)
{

m = (6*h)/(3*h+t);
w_eff = w+(t/Math.PI)*Math.log(Math.exp(1)/Math.sqrt(Math.pow((t/(4*h+t)),2)+Math.pow(((Math.PI*t)/(4*(w+1.1*t))),m)));
no = 377;
zo_ss = (no/(2*Math.PI*Math.sqrt(er)))*Math.log(1+((8*h)/(Math.PI*w_eff))*(((16*h)/(Math.PI*w_eff))+Math.sqrt(Math.pow(((16*h)/(Math.PI*w_eff)),2)+6.27)));
b = 2*h+t;
D = (w/2)*(1+(t/(Math.PI*w))*(1+Math.log((4*Math.PI*w)/t))+.551*Math.pow((t/w),2));
zo_ss_t2 = (60/Math.sqrt(er))*Math.log((4*b)/(Math.PI*D));
theta = ((2*b)/(b-t))*Math.log((2*b-t)/(b-t))-(t/(b-t))*Math.log((2*b*t-Math.pow(t,2))/(Math.pow((b-t),2)));
zo_ss_w2 = 94.15/(((w/b)/(1-(t/b)))+(theta/Math.PI));
if (((w/b)<.35) ||((t/b) <= .25)||((t/w) <=.11)){
	zo = zo_ss_t2;}
else{
	zo = zo_ss_w2;}

return {zo:zo};
}

function ecs_stripline_calc(w,t,s,b,er)
{
var no =377;
var h=(b-t)/2;
var ke = tanh((Math.PI*w)/(2*b))*tanh((Math.PI/2)*(w+s)/b);
var ko = tanh((Math.PI*w)/(2*b))*coth((Math.PI/2)*(w+s)/b);
var ke_prime = Math.sqrt(1-Math.pow(ke,2));
var ko_prime = Math.sqrt(1-Math.pow(ko,2));
var Z0_e_0=(30*Math.PI/Math.sqrt(er))*(eliptical(ke_prime)/eliptical(ke));
var Z0_o_0=(30*Math.PI/Math.sqrt(er))*(eliptical(ko_prime)/eliptical(ko));
var Zo_ss= sym_stripline(w,t,h,er);
var cf_tb=(.0885*er/Math.PI)*(((2*b)/(b-t))*Math.log((2*b-t)/(b-t))-(t/(b-t))*Math.log((Math.pow(b,2)/Math.pow((b-t),2))-1));
var cf_0=(.0885*er/Math.PI)*2*Math.log(2);
var k_ideal = sech((Math.PI*w)/(2*b));
var k_ideal_prime = tanh((Math.PI*w)/(2*b));
var Z0_ideal = (no/(4*Math.sqrt(er)))*(eliptical(k_ideal)/eliptical(k_ideal_prime));
var temp1 = 1/(((1/Zo_ss.zo))-(cf_tb/cf_0)*((1/(Z0_o_0))-(1/(Z0_ideal))));
var temp2 = s/t;
var zo_odd_1 = 1/(((1/Zo_ss.zo))-(cf_tb/cf_0)*((1/(Z0_o_0))-(1/(Z0_ideal))));
var zo_odd_2 = 1/((2/(Z0_o_0))-(1/Z0_ideal)-(2/(no*er))*(cf_tb-cf_0-(er/s)));
var zo_odd=0;

if ((s/t)>=(5)){
	zo_odd = zo_odd_1;
	}
else{
	zo_odd = zo_odd_2;
	}


var zo_even = 1/((1/(Zo_ss.zo))-(cf_tb/cf_0)*((1/(Z0_ideal))-(1/(Z0_e_0))));
var zo_common = zo_even/2;
var zo_diff = zo_odd*2;
var A1 = 1/Zo_ss.zo;
var A2 = cf_tb/cf_0;
var A3_even = 1/Z0_ideal;
var A3_odd = 1/Z0_o_0;
var A4_even = 1/Z0_e_0;
var A4_odd = 1/Z0_ideal;
return{zo_odd:zo_odd,zo_even:zo_even,zo_common:zo_common,zo_diff:zo_diff};
}




function emb_microstrip()
{
var t = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var h1 = parseFloat(document.getElementById("sub_height1").value)*document.getElementById("sub_height1_unit").value;//substrate height
var h2 = parseFloat(document.getElementById("sub_height2").value)*document.getElementById("sub_height2_unit").value;//substrate height
var w = parseFloat(document.getElementById("width").value)*document.getElementById("width_unit").value;//trace width
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=embedded_microstrip(w,t,h1,h2,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms"
}

function embedded_microstrip(w,t,h1,h2,er)
{
var zo_surf = microstrip_calc(w,t,h1,er);
var b = h2-h1;
var zo_embed = zo_surf.zo*(1/Math.sqrt((Math.exp((-2*b)/h1))+(er/zo_surf.er_eff)*(1-Math.exp((-2*b)/h1))))

return {zo:zo_embed};
}

function microstrip_calc(w,t,h,er)
{
var er_temp1=((er+1)/2)+((er-1)/2)*(Math.sqrt(w/(w+12*h))+0.04*Math.pow((1-w/h),2));
var er_temp2=((er+1)/2)+((er-1)/2)*(Math.sqrt(w/(w+12*h)));
var er_eff=0;
if ((w/h)<1){
	er_eff=er_temp1;
	}
	else{
	er_eff=er_temp2;
	}
var w_eff=w+(t/Math.PI)*Math.log(4*Math.exp(1)/Math.sqrt(Math.pow((t/h),2)+Math.pow((t/(w*Math.PI+1.1*t*Math.PI)),2)))*((er_eff+1)/2*er_eff);
var no=377;
var temp1 = Math.sqrt((16*Math.pow((h/w_eff),2)*Math.pow(((14*er_eff+8)/(11*er_eff)),2)+((er_eff+1)/(2*er_eff))*Math.pow((Math.PI),2)));
var temp2 = 4*(h/w_eff)*((14*er_eff+8)/(11*er_eff));
var temp3 = 4*(h/w_eff);
var temp4 = (no/(2*Math.PI*Math.sqrt(2)*Math.sqrt(er_eff+1)))
var zo=temp4*Math.log(1+temp3*(temp2+temp1));
return {zo:zo,er_eff:er_eff};
}




function microstrip()
{
var t = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var h = parseFloat(document.getElementById("sub_height").value)*document.getElementById("sub_height_unit").value;//substrate height
var w = parseFloat(document.getElementById("width").value)*document.getElementById("width_unit").value;//trace width
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=microstrip_calc(w,t,h,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms"
}




function stripline()
{
var t = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var h = parseFloat(document.getElementById("sub_height").value)*document.getElementById("sub_height_unit").value;//substrate height
var w = parseFloat(document.getElementById("width").value)*document.getElementById("width_unit").value;//trace width
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=sym_stripline(w,t,h,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms"
}

function sym_stripline(w,t,h,er)
{
var zo=0;
m = (6*h)/(3*h+t);
w_eff = w+(t/Math.PI)*Math.log(Math.exp(1)/Math.sqrt(Math.pow((t/(4*h+t)),2)+Math.pow(((Math.PI*t)/(4*(w+1.1*t))),m)));
no = 377;
zo_ss = (no/(2*Math.PI*Math.sqrt(er)))*Math.log(1+((8*h)/(Math.PI*w_eff))*(((16*h)/(Math.PI*w_eff))+Math.sqrt(Math.pow(((16*h)/(Math.PI*w_eff)),2)+6.27)));
b = 2*h+t;
D = (w/2)*(1+(t/(Math.PI*w))*(1+Math.log((4*Math.PI*w)/t))+.551*Math.pow((t/w),2));
zo_ss_t2 = (60/Math.sqrt(er))*Math.log((4*b)/(Math.PI*D));
theta = ((2*b)/(b-t))*Math.log((2*b-t)/(b-t))-(t/(b-t))*Math.log((2*b*t-Math.pow(t,2))/(Math.pow((b-t),2)));
zo_ss_w2 = 94.15/(((w/b)/(1-(t/b)))+(theta/Math.PI));
if (((w/b)<.35)||((t/b) <= .25)||((t/w) <=.11)){
	 zo = zo_ss_t2;}
else{
	zo = zo_ss_w2;}

return {zo:zo};
}




function twist_pair()
{
var d = parseFloat(document.getElementById("d").value)*document.getElementById("d_unit").value;//
var s = parseFloat(document.getElementById("s").value)*document.getElementById("s_unit").value;//
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=twist_pair_calc(d,s,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms";
document.getElementById("delay_value").innerHTML=trace.delay.toPrecision(3)+"&nbsp;ns/in";
document.getElementById("capacitance_value").innerHTML=trace.cap.toPrecision(3)+"&nbsp;pF/in";
document.getElementById("inductance_value").innerHTML=trace.ind.toPrecision(3)+"&nbsp;nH/in";
}
function twist_pair_calc(d,s,er)
{
var zo=(120/Math.sqrt(er))*Math.log(2*s/d);
var delay=(84.72e-12*Math.sqrt(er))/1e-9;
var ind=(10.16e-9*Math.log(2*s/d))/1e-9;
var cap=((.7065e-12/Math.log(2*s/d))*er)/1e-12;
return{zo:zo,delay:delay,ind:ind,cap:cap};
}




function wire_microstrip()
{
var d = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var h = parseFloat(document.getElementById("sub_height").value)*document.getElementById("sub_height_unit").value;//substrate height
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=wire_microstrip_calc(d,h,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms"
}

function wire_microstrip_calc(d,h,er)
{
no = 377;
er_eff1 = ((er+1)/2)+((er-1)/2)*(Math.sqrt(d/(d+12*h))+.04*Math.pow((1-(d/h)),2));
er_eff2 = ((er+1)/2)+((er-1)/2)*(Math.sqrt(d/(d+12*h)));
if((d/h)<1){
	er_eff = er_eff1;}
else{
	er_eff = er_eff2;}
temp = (2*h+d)/d;
zo_wm = (no/(2*Math.PI*Math.sqrt(er_eff)))*Math.log(temp + Math.sqrt(temp * temp - 1));
return{zo:zo_wm};
}




function wire_stripline()
{
var d = parseFloat(document.getElementById("thick").value)*document.getElementById("thick_unit").value;//trace thickness
var h = parseFloat(document.getElementById("sub_height").value)*document.getElementById("sub_height_unit").value;//substrate height
var er = parseFloat(document.getElementById("er").value);//dielectric constant. no unit choices
trace=wire_stripline_calc(d,h,er);
document.getElementById("zo_value").innerHTML=trace.zo.toPrecision(3)+"&nbsp;Ohms"
}

function wire_stripline_calc(d,h,er)
{
no = 377;
zo_ws = (no/(2*Math.PI*Math.sqrt(er)))*Math.log((4*h)/(Math.PI*d));
return{zo:zo_ws};
}





