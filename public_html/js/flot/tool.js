$(function() {
    
  $("#r").val(200);  //sample for setting init val
  $("#c").val(1e-7);  //sample for setting init val
 
  var latestPosition = null;  // Global for Cursor Position
  var updateCursorTimout = null; // Global for Timer Event 


function calc_impedance(f,r,c)
{
  var w=2*Math.PI*f
  var mag = Math.sqrt(r*r+1/(w*w*c*c));
  var phase = ((Math.atan(-1/(w*c*r)))*(360/(2*Math.PI)));
  var real = r;
  var imag = -1/(w*c);
return {mag:mag,phase:phase,real:real,imag:imag};
}

function engFormat(v){
  
  if(v>=1e12)
    v=(v/1e12).toFixed(2)+'T';
  else if(v>=1e9)
    v=(v/1e9).toFixed(2)+'G';
  else if(v>=1e6)
    v=(v/1e6).toFixed(2)+'M';
  else if(v>=1e3)
    v=(v/1e3).toFixed(2)+'k';
  else{
    v=parseFloat(v);
    v=v.toFixed(2);
  }
  return v
}

function magPhase(){

  var d_mag = [];
  var d_phase = [];
  var r = parseFloat($("#r").val());
  var c = parseFloat($("#c").val()); 
  
  var impedance;
  
  var chartoptions_mag = {
    crosshair: { mode: "x", color: "#ff0000"},
          lines: { show: true  },
          legend: {margin:10},
          xaxis: { ticks: 6, base: 10, tickDecimals: 0  },
          yaxis: { ticks: 4, base:10, tickDecimals:0},
          y2axis: { ticks: [180,90,0,-90,-180], min:-180, max: 180,tickFormatter: function (v, axis) { return (v+'&deg;') }},
          grid: { hoverable: true, clickable: true , color: "#999",autoHighlight:false}
  	};
  	
  /*
  When using Log scale the charts look best when the number of data points per decade are the same.
  To do this the points of the loop should start at
  1 - 10
  2 - 100
  3 - 1000
  4 - 1e4
  5 - 1e5
  6 - 1e6
  And the number of points per decade are determined by the step size
  10 - .1
  100 - .01
  Then we use a function on this number that is f=10^n
  
  */
  
  for (var i = 1; i <= 6.1; i += .1){
  	f=Math.pow(10,i)
  	impedance = calc_impedance(f,r,c);
    d_mag.push([f, impedance.mag]);
  	d_phase.push([f, impedance.phase]);
  }
  
  var data_set1 = {label: "Mag",data: d_mag}; 
  var data_set2 = {label: "Phase",data: d_phase, yaxis: 2 }; 
  
  $.plot($("#chart_mag"),  [ data_set1,data_set2], chartoptions_mag);
  
  $("#chart_mag").unbind("plothover"); //Clear any previous Binds
  $("#chart_mag").bind("plothover", function (event, pos, item) {
       latestPosition=pos;
       if(!updateCursorTimout)
         updateCursorTimout = setTimeout(updateMagPhaseData,50);
  });
}

function updateMagPhaseData(){
  updateCursorTimout = null;
  if(latestPosition!=null){
    var pos = latestPosition;
    var r = parseFloat($("#r").val());
    var c = parseFloat($("#c").val()); 
    var f=(Math.pow(10,pos.x)).toFixed(2);
    $("#outputLabel_1").text("Freq:");
    $("#outputLabel_2").text("Mag:");
    $("#outputLabel_3").text("Phase:");
    
    $("#outputVal_1").html(engFormat(f)+'Hz');
    $("#outputVal_2").html(engFormat(calc_impedance(f,r,c).mag));
    $("#outputVal_3").html((calc_impedance(f,r,c).phase).toFixed(2)+'&deg;');
  }
}

function realComplex(){

  var d_real = [];
  var d_imag = [];
  var r = parseFloat($("#r").val());
  var c = parseFloat($("#c").val()); 

  
  var impedance;
  
  var chartoptions_mag = {
    crosshair: { mode: "x", color: "#ff0000"},
          lines: { show: true  },
          legend: {margin:10},
          xaxis: { ticks: 6, base: 10, tickDecimals: 0  },
          yaxis: { ticks: 4},
          y2axis: { ticks: 4,min:0},
          grid: { hoverable: true, clickable: true , color: "#999",autoHighlight:false}
  	};

  
  for (var i = 3; i <= 6.1; i += .1){
  	f=Math.pow(10,i)
  	impedance = calc_impedance(f,r,c);
  	d_real.push([f, impedance.real]);
  	d_imag.push([f, (impedance.imag)]);

  }
  
  var data_set1 = {label: "Img",data: d_imag}; 
  var data_set2 = {label: "Real",data: d_real, yaxis: 2 }; 
  
  $.plot($("#chart_mag"),  [ data_set1,data_set2], chartoptions_mag);
  
  $("#chart_mag").unbind("plothover"); //Clear any previous Binds
  $("#chart_mag").bind("plothover", function (event, pos, item) {
       latestPosition=pos;
       if(!updateCursorTimout)
         updateCursorTimout = setTimeout(updateRealComplexData,50);
  });

}

function updateRealComplexData(){
  updateCursorTimout = null;
  if(latestPosition!=null){
    var pos = latestPosition;
    var f=(Math.pow(10,pos.x)).toFixed(2);
    var r = parseFloat($("#r").val());
    var c = parseFloat($("#c").val()); 
    
    $("#outputLabel_1").text("Freq:");
    $("#outputLabel_2").text("Real:");
    $("#outputLabel_3").text("Img:");
    
    $("#outputVal_1").html(engFormat(f)+'Hz');
    $("#outputVal_2").html(engFormat(calc_impedance(f,r,c).real)+'&#8486;');
    $("#outputVal_3").html(engFormat((calc_impedance(f,r,c).imag))+'&#8486;');
  }
}

// Bind KeyUp Input Box Event to make plots dynamic

$('.fieldInput').keyup(function () {
     $('#'+activePlot).trigger('click');
    
})


// Button Hovers and Click Functions

$('.chartButtonWrap li').hover(
  function () {
    if($(this).attr('id')!=activePlot)
      $(this).css("background-color","#ccc");
  }, 
  function () {
      if($(this).attr('id')!=activePlot){
     $(this).css("background-color", "#e6e6e6");
      }
    }
);

$('.chartButtonWrap li').click(
  function(){
    $('.chartButtonWrap li').css("background-color","#e6e6e6");
    $(this).css("background-color","#ccc");
});

var activePlot;  // Global Keep track of Active Plot

$("#magPhase").click(
  function(){
   magPhase();
   updateMagPhaseData()
   activePlot=$(this).attr('id');
  });

$("#realComplex").click(
  function(){
   realComplex();
   updateRealComplexData();
   activePlot=$(this).attr('id');
  });

 $("#magPhase").trigger('click');


});



