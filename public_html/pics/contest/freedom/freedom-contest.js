$(document).ready(function(){
    $("#clearit").click(function(){
        $('input[type="text"]').val("");
        $("#accept").attr("checked",false);
    });

    $("#survey-form").submit(function(event){
    	var plink = $("#plink").val();

        event.preventDefault();
        var data_value = $(this).serialize(); 
            $.ajax({
                type: 'post',
                url:   '/freedomcontest/form_process/',
                data: data_value,
                success: function(data) {
                    if(data){
                        if( $("#headerMessage").hasClass('successBg')) {
                            $("#headerMessage").removeClass('successBg');
                        }

                        $("#headerMessage").addClass("errorBg").show().html("<span>" +data + "</span>");
                        $('html, body').animate({
                                scrollTop: $('#headerMessage').offset().top - 200
                        });

                    } else {
                    	(plink == "phase1") ? window.location.href ='/freedomcontest/finish' : window.location.href ='/freedomcontest/finish2';
                    }
                },error: function(data) {
                        console.log(data);
                 }            
               
            }); 
            
        });
});