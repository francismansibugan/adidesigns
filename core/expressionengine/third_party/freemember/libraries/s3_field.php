<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class S3_field
{
	public $settings = array();
	public $s3 = FALSE;
	public $custom_subdomain = array("s.eeweb.com.s3.amazonaws.com" => "s.eeweb.com");

	public function __construct()
	{
		$this->settings = array(
			's3_key' => 'AKIAJK3RBUFDSCHO7HZA',
			's3_secret' => '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',
			's3_bucket' => 'eeweb-europe',
			'acl_public_read' => 'yes',
			'cache_control' => '10',
			'max_filesize' => '10000000',
			'filename' => 'avatars/%m/%t-%f.%e',
			'image_width' => '1024',
			'image_height' => '768',
			'image_quality' => '95',
			'image_resize_method' => 'fit',
			'image_upscale' => 'yes',
			'thumbname' => 'avatars/%m/_thumbs/%t-%f.%e',
			'thumb_width' => '100',
			'thumb_height' => '100',
			'thumb_quality' => '90',
			'thumb_resize_method' => 'fit',
			'thumb_upscale' => 'yes'
		);

		ee()->load->library('S3');
	}

	public function build_field($name, $data)
	{
		$cols = array(
			'filename',
			'avatar_filename',
			'url',
			'filesize',
			'mime',
			'avatar_width',
			'avatar_height',
			's3_thumbname',
			'thumb_url',
			'thumb_filesize',
			'thumb_width',
			'thumb_height'
		);

		foreach ($cols as $col)
		{
			$data[$col] = (isset($data[$col])) ? $data[$col] : "";
		}
		
		$form = "";
		$form .= "<strong>Current Profile Image:</strong>";

		if ($data['avatar_filename'] != "")
		{
			$file_url = $this->_custom_s3_domain($data['avatar_filename']);
			$thumb_url = $this->_custom_s3_domain($data['thumb_url']);

			if ($this->settings['acl_public_read'] != "yes")
			{
				if (!$this->s3)
				{
					$this->s3 = new S3($this->settings['s3_key'], $this->settings['s3_secret']);
				}
				$file_url = S3::getAuthenticatedUrl($this->settings['s3_bucket'], $this->s3_filename("", "", $data, $this->settings), 60 * 60);
				if ($thumb_url != "")
				{
					$thumb_url = S3::getAuthenticatedUrl($this->settings['s3_bucket'], $this->thumb_s3_filename("", "", $data, $this->settings), 60 * 60);
				}
			}

			if ($file_url != "")
			{
				$form .= "<p><a href=\"$file_url\" target=\"_blank\"><img src=\"$file_url\" width=\"100\" height=\"100\" style=\"border: none;\" /></a></p>";
			}
			else
			{
				$form .= "<p><a href=\"$file_url\" target=\"_blank\">{$data['filename']}</a> (" . $this->_format_bytes($data['filesize']) . ")</p>";
			}
			// $form .= "<strong>Current Profile Image:</strong><br />";
		}

		$form .= "<input type='file' name='$name' id='$name' class='s3_uploader' />\n";

		foreach ($data as $k => $v)
		{
			$form .= "<input type='hidden' name='{$name}[$k]' value='$v' />\n";
		}

		return $form;
	}

	function _custom_s3_domain($url)
	{
		$search = array_keys($this->custom_subdomain);
		$replace = array_values($this->custom_subdomain);
		return str_replace($search, $replace, $url);
	}

	function s3_filename($params, $tagdata, $field_data, $field_settings)
	{
		if (!is_array($field_data))
		{
			$field_data = @unserialize($field_data);
		}
		if (is_array($field_data) && isset($field_data['avatar_filename']))
		{
			return $field_data['avatar_filename'];
		}
		else if (is_array($field_data) && isset($field_data['url']))
		{
			$file = strrchr($field_data['url'], "/");
			if ($file)
			{
				return trim($file, "/");
			}
		}
		else
		{
			return '';
		}
	}

	function thumb_s3_filename($params, $tagdata, $field_data, $field_settings)
	{
		if (!is_array($field_data))
		{
			$field_data = @unserialize($field_data);
		}
		if (is_array($field_data) && isset($field_data['s3_thumbname']))
		{
			return $field_data['s3_thumbname'];
		}
		else if (is_array($field_data) && isset($field_data['thumb_url']))
		{
			$file = strrchr($field_data['thumb_url'], "/");
			if ($file)
			{
				return trim($file, "/");
			}
		}
		else
		{
			return '';
		}
	}

	function _upload_to_s3($file_info)
	{
		$ret = array();
		$aws_key = $this->settings['s3_key'];
		$aws_secret = $this->settings['s3_secret'];
		$aws_bucket = $this->settings['s3_bucket'];
		$source_file = $file_info['tmp_name'];
		$file_type = $file_info['type'];
		$pathinfo = pathinfo($file_info['name']);
		$filename = $this->pathinfo_filename($file_info['name']);
		$object_fmt = "%t-%f.%e";
		$thumb_fmt = "%t-%f-thumb.%e";

		if (isset($this->settings['filename']) && $this->settings['filename'] != "")
		{
			$object_fmt = $this->settings['filename'];
		}

		if (isset($this->settings['thumbname']) && $this->settings['thumbname'] != "")
		{
			$thumb_fmt = $this->settings['thumbname'];
		}

		$replacements = array(
			"%t" => time(),
			"%Y" => date("Y", time()),
			"%M" => date("m", time()),
			"%D" => date("d", time()),
			"%f" => $filename,
			"%e" => $pathinfo['extension'],
			"%m" =>  ee()->session->userdata['member_id'],
			"%u" =>  ee()->session->userdata['username'],
			"%s" =>  ee()->session->userdata['screen_name'],
			"%g" =>  ee()->session->userdata['group_title'],
		);

		$aws_object = $object_fmt;
		$aws_thumb = $thumb_fmt;

		foreach ($replacements as $k => $v)
		{
			$aws_object = str_replace($k, $v, $aws_object);
			$aws_thumb = str_replace($k, $v, $aws_thumb);
		}

		$aws_object = preg_replace("/[^\/_a-z0-9.]+/i", "-", $aws_object);
		$aws_thumb = preg_replace("/[^\/_a-z0-9.]+/i", "-", $aws_thumb);

		$ret['filesize'] = filesize($source_file);

		$pathinfo = pathinfo($source_file);
		$filename = $this->pathinfo_filename($source_file);
		$resize_file = "{$pathinfo['dirname']}/" . time() . "$filename";
		$thumb_file = "{$pathinfo['dirname']}/" . time() . "$filename-thumb";

		$image = $this->_resize_image($source_file, $resize_file, $this->settings['image_width'], $this->settings['image_height'], $this->settings['image_quality'], $this->settings['image_resize_method'], $this->settings['image_upscale']);

		if ($image !== false)
		{
			$ret['image_width'] = $image['width'];
			$ret['image_height'] = $image['height'];
			$ret['filesize'] = filesize($resize_file);
		}

		$thumb = $this->_resize_image($source_file, $thumb_file, $this->settings['thumb_width'], $this->settings['thumb_height'], $this->settings['thumb_quality'], $this->settings['thumb_resize_method'], $this->settings['thumb_upscale']);

		// only checks filesize if we didn't resize the image...
		// not fair to say we'll resize and then fail due to the file still being too big
		if ($this->settings['max_filesize'] > 0 && $image === false)
		{
			if (filesize($source_file) > $this->settings['max_filesize'])
			{
				return "";
			}
		}

		if (!$this->s3)
		{
			$this->s3 = new S3($aws_key, $aws_secret);
		}

		$acl = $this->settings['acl_public_read'] == "yes" ? S3::ACL_PUBLIC_READ : S3::ACL_PRIVATE;

		$cache_days = $this->settings['cache_control'] ? $this->settings['cache_control'] : 0;

		$request_headers = array(
			"Content-Type" => $file_type,
			"Cache-Control" => "max-age=" . $cache_days * 24 * 60 * 60
		);

		$input = false;
		if ($image !== false)
		{
			$input = $this->s3->inputResource(fopen($resize_file, "rb"), filesize($resize_file));
		}
		else
		{
			$input = $this->s3->inputResource(fopen($source_file, "rb"), filesize($source_file));
		}
		if ($this->s3->putObject($input, $aws_bucket, $aws_object, $acl, array(), $request_headers))
		{
			$ret['s3_filename'] = $aws_object;
			$ret['filename'] = $file_info['name'];
			$ret['mime'] = $file_info['type'];
			$ret['url'] = "http://{$aws_bucket}.s3.amazonaws.com/{$aws_object}";
		}
		else
		{
			return "";
		}

		if ($thumb !== false)
		{
			$input = $this->s3->inputResource(fopen($thumb_file, "rb"), filesize($thumb_file));

			if ($this->s3->putObject($input, $aws_bucket, $aws_thumb, $acl, array(), $request_headers))
			{
				$ret['s3_thumbname'] = $aws_thumb;
				$ret['thumb_url'] = "http://{$aws_bucket}.s3.amazonaws.com/{$aws_thumb}";
				$ret['thumb_width'] = $thumb['width'];
				$ret['thumb_height'] = $thumb['height'];
				$ret['thumb_filesize'] = filesize($thumb_file);
			}
		}

		if (file_exists($resize_file))
		{
			unlink($resize_file);
		}
		if (file_exists($thumb_file))
		{
			unlink($thumb_file);
		}

		return $ret;
	}

	function pathinfo_filename($file)
	{
		if (defined('PATHINFO_FILENAME'))
		{
			return pathinfo($file, PATHINFO_FILENAME);
		}
		else if (strstr($file, '.'))
		{
			return substr($file, 0, strrpos($file, '.'));
		}
		else
		{
			return $file;
		}
	}

	function _resize_image($source_file, $dest_file, $width, $height, $quality, $method = "fit", $upscale = "no")
	{
		$ret = array();
		$image = false;
		switch(exif_imagetype($source_file))
		{
			case IMAGETYPE_GIF :
				$image = imagecreatefromgif($source_file);
				break;
			case IMAGETYPE_JPEG :
				$image = imagecreatefromjpeg($source_file);
				break;
			case IMAGETYPE_PNG :
				$image = imagecreatefrompng($source_file);
				break;
			case IMAGETYPE_BMP :
				// don't think PHP can create an image from a BMP
				// will need to find a third-party function for this if anyone cares about BMPs
				// anymore
				break;
		}

		// file not a supported image type
		if ($image === false)
		{
			return false;
		}

		// invalid dimensions specified, don't bother attempting to resize
		// note: possible that only width or height is specified, don't need both
		if ($width <= 0 && $height <= 0)
		{
			return false;
		}

		$imagesize = getimagesize($source_file);

		if ($imagesize === false)
		{
			return false;
		}

		$max_width = PHP_INT_MAX;
		$max_height = PHP_INT_MAX;
		$image_width = $imagesize[0];
		$image_height = $imagesize[1];

		if ($width > 0)
		{
			$max_width = $width;
		}
		if ($height > 0)
		{
			$max_height = $height;
		}

		$dim = $this->_get_resize_dimensions($image_width, $image_height, $max_width, $max_height, $method, $upscale);

		if ($dim['copy'])
		{
			copy($source_file, $dest_file);
			$ret['width'] = round($image_width);
			$ret['height'] = round($image_height);
		}
		else
		{
			$resized_image = imagecreatetruecolor($dim['dw'], $dim['dh']);
			$alpha_image = false;

			switch($imagesize[2])
			{
				case IMAGETYPE_GIF :
					$this->_set_transparency($resized_image, $image);
					break;
				case IMAGETYPE_PNG :
					imagealphablending($resized_image, false);
					imagesavealpha($resized_image, true);

					$alpha_image = imagecreatetruecolor($image_width, $image_height);
					imagealphablending($alpha_image, false);
					imagesavealpha($alpha_image, true);

					for ($x = 0; $x < $image_width; $x++)
					{
						for ($y = 0; $y < $image_height; $y++)
						{
							$alpha = (imagecolorat($image, $x, $y)>>24) & 0xFF;
							$color = imagecolorallocatealpha($alpha_image, 0, 0, 0, $alpha);
							imagesetpixel($alpha_image, $x, $y, $color);
						}
					}

					break;
			}

			imagegammacorrect($image, 2.2, 1.0);
			imagecopyresampled($resized_image, $image, 0, 0, $dim['sx'], $dim['sy'], $dim['dw'], $dim['dh'], $dim['sw'], $dim['sh']);
			imagegammacorrect($resized_image, 1.0, 2.2);

			imagedestroy($image);

			switch($imagesize[2])
			{
				case IMAGETYPE_GIF :
					imagegif($resized_image, $dest_file);
					break;
				case IMAGETYPE_JPEG :
					imagejpeg($resized_image, $dest_file);
					break;
				case IMAGETYPE_PNG :
					$alpha_resized_image = imagecreatetruecolor($dim['dw'], $dim['dh']);
					imagealphablending($alpha_resized_image, false);
					imagesavealpha($alpha_resized_image, true);
					imagecopyresampled($alpha_resized_image, $alpha_image, 0, 0, $dim['sx'], $dim['sy'], $dim['dw'], $dim['dh'], $dim['sw'], $dim['sh']);

					for ($x = 0; $x < $dim['dw']; $x++)
					{
						for ($y = 0; $y < $dim['dh']; $y++)
						{
							$alpha = (imagecolorat($alpha_resized_image, $x, $y)>>24) & 0xFF;
							$rgb = imagecolorat($resized_image, $x, $y);
							$r = ($rgb>>16) & 0xFF;
							$g = ($rgb>>8) & 0xFF;
							$b = $rgb & 0xFF;
							$color = imagecolorallocatealpha($resized_image, $r, $g, $b, $alpha);
							imagesetpixel($resized_image, $x, $y, $color);
						}
					}

					imagedestroy($alpha_image);
					imagedestroy($alpha_resized_image);

					imagepng($resized_image, $dest_file);
					break;
			}
			imagedestroy($resized_image);

			$ret['width'] = $dim['dw'];
			$ret['height'] = $dim['dh'];
		}

		return $ret;
	}

	function _get_resize_dimensions($iw, $ih, $mw, $mh, $method = "fit", $upscale = "no")
	{
		$r = array();
		$r['copy'] = false;

		if ($method == "fill" && $mw < PHP_INT_MAX && $mh < PHP_INT_MAX)
		{
			if ($upscale == "yes" || ($iw > $mw && $ih > $mh))
			{
				$ratio_width = $iw / $mw;
				$ratio_height = $ih / $mh;

				$scale_by = $ratio_width <= $ratio_height ? $ratio_width : $ratio_height;

				$w = $iw / $scale_by;
				$h = $ih / $scale_by;

				$r['sx'] = floor(($w - $mw) / 2 * $scale_by);
				$r['sy'] = floor(($h - $mh) / 2 * $scale_by);

				$r['dw'] = $mw;
				$r['dh'] = $mh;

				$r['sw'] = floor($mw * $scale_by);
				$r['sh'] = floor($mh * $scale_by);

				return $r;
			}
			else
			{
				$r['copy'] = true;
				return $r;
			}
		}
		else if ($method == "stretch" && $mw < PHP_INT_MAX && $mh < PHP_INT_MAX)
		{
			if ($upscale == "yes" || $iw > $mw || $ih > mh)
			{
				$r['sx'] = 0;
				$r['sy'] = 0;

				$r['dw'] = $mw;
				$r['dh'] = $mh;

				$r['sw'] = $iw;
				$r['sh'] = $ih;

				return $r;
			}
			else
			{
				$r['copy'] = true;
				return $r;
			}
		}
		else
		{
			if ($upscale == "yes" || $iw > $mw || $ih > $mh)
			{
				$ratio_width = $iw / $mw;
				$ratio_height = $ih / $mh;

				$scale_by = $ratio_width >= $ratio_height ? $ratio_width : $ratio_height;

				$r['sx'] = 0;
				$r['sy'] = 0;

				$r['dw'] = floor($iw / $scale_by);
				$r['dh'] = floor($ih / $scale_by);

				$r['sw'] = $iw;
				$r['sh'] = $ih;

				return $r;
			}
			else
			{
				$r['copy'] = true;
				return $r;
			}
		}
	}

	function _set_transparency($new_image, $image_source)
	{
		$transparencyIndex = imagecolortransparent($image_source);
		$transparencyColor = array(
			'red' => 255,
			'green' => 255,
			'blue' => 255
		);

		if ($transparencyIndex >= 0)
		{
			$transparencyColor = imagecolorsforindex($image_source, $transparencyIndex);
		}

		$transparencyIndex = imagecolorallocate($new_image, $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue']);
		imagefill($new_image, 0, 0, $transparencyIndex);
		imagecolortransparent($new_image, $transparencyIndex);
	}

	function _format_bytes($bytes, $precision = 2)
	{
		$units = array(
			'B',
			'KB',
			'MB',
			'GB',
			'TB'
		);

		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		$bytes /= pow(1024, $pow);

		return round($bytes, $precision) . ' ' . $units[$pow];
	}

}
