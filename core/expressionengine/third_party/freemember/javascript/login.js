	$(function() {
		input = $("#top-login input");

		input_home = $("#side-user-login input");

		init_tooltip();

		$(document).click(function(event) { 
			if(!$(event.target).closest('.login').length) {
				if($('.login').is(":visible")) {
					remove_login();
				}
			}
		})

		$("#login_button").click(function(e){
			e.preventDefault();
			validate_login();
		});

		$("#sign-in").click(function(e){
			e.preventDefault();
			validate_login_home();
		});

		$('.login-dropdown').mouseenter(function() {
			$('.login').show('fast');
		});

		function remove_login(){
			$('.login').attr("id","").hide('fast');
			$("#username").tooltipster('hide');
			$("#password").tooltipster('hide');
		}

		function init_tooltip() {
			$("#username").tooltipster({
				content: '' ,
				animation: 'grow',
				touchDevices: true,
				position:'left',
				trigger:'',
			});
			$("#password").tooltipster({
				content: '' ,
				animation: 'grow',
				touchDevices: true,
				position:'left',
				trigger:''
			});
			$("#username_home").tooltipster({
				content: '' ,
				animation: 'grow',
				touchDevices: true,
				position:'left',
				trigger:'',
			});
			$("#password_home").tooltipster({
				content: '' ,
				animation: 'grow',
				touchDevices: true,
				position:'left',
				trigger:''
			});

			input.each(function(k,v){
				$(v).unbind('keypress');
			}).focus(function(){
				$('.login').attr("id","dp_log");
				$("#username").tooltipster('hide');
				$("#password").tooltipster('hide');
			}).blur(function(){
				$('.login').attr("id","");
				$("#username").tooltipster('hide');
				$("#password").tooltipster('hide');

			});

			input_home.each(function(k,v){
				$(v).unbind('keypress');
			}).focus(function(){
				$("#username_home").tooltipster('hide');
				$("#password_home").tooltipster('hide');
			}).blur(function(){
				$("#username_home").tooltipster('hide');
				$("#password_home").tooltipster('hide');

			});

		} 
	
	function validate_login(){
		var form = $('#top-login').serialize();
		var tempStr ="";
		parts = form.split('&');
		for (var i = 0; i < parts.length; i++) {
			if((parts[i].indexOf("ACT") != -1)) {
				parts[i] = parts[i].replace(/ACT/g, 'action');
			}
			tempStr += parts[i]+"&";
		}
		form = tempStr.slice(0, -1);
		$.ajax({
			dataType: "json",
			type: "POST",
			url: "{{ajax_url}}",
			data: form,
			beforeSend: function() {

			},
			success: function(data) {
				if(data.success) {
					window.location.href = data.url;
				} else {
					input = $("#top-login input");
					$('.login').attr("id","dp_log");
					$('#username').tooltipster('content', data.username);
					$('#password').tooltipster('content', data.password);
					if(data.username)
						$("#username").tooltipster('show');
					if(data.password)
						$("#password").tooltipster('show');
				}
			},
			error: function(xhr, status, err) {
				console.log("xhr: ", xhr);
				console.log("status: ", status);
				console.log("err: ", err);
			},
			complete: function() {

			}
		});
	}

	function validate_login_home(){
		var form = $('#side-user-login').serialize();
		var tempStr ="";
		parts = form.split('&');
		for (var i = 0; i < parts.length; i++) {
			if((parts[i].indexOf("ACT") != -1)) {
				parts[i] = parts[i].replace(/ACT/g, 'action');
			}
			tempStr += parts[i]+"&";
		}
		form = tempStr.slice(0, -1);
		$.ajax({
			dataType: "json",
			type: "POST",
			url: "{{ajax_url}}",
			data: form,
			beforeSend: function() {

			},
			success: function(data) {
				if(data.success) {
					window.location.href = data.url;
				} else {
					input_home = $("#side-user-login input");
					$('#username_home').tooltipster('content', data.username);
					$('#password_home').tooltipster('content', data.password);
					if(data.username)
						$("#username_home").tooltipster('show');
					if(data.password)
						$("#password_home").tooltipster('show');
				}
			},
			error: function(xhr, status, err) {
				console.log("xhr: ", xhr);
				console.log("status: ", status);
				console.log("err: ", err);
			}
		});
	}
});