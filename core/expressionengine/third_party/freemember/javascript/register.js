$(function() {
	var isNextBtnClicked = false;
	var isNextBtn2Clicked = false;
	var page1, page2, page3, page4;

	$('#interests_info').hide();

	$('#job_info').hide();
	// $('#basic_info').hide();

	$('#wrapper_business').hide();
	
	$('#business_job_info').hide();


	$('#engineer').change(function () {
		if ($(this).val() == 'on') {
			$('#wrapper_engineer').css('display', 'block');
			$('#wrapper_business').css('display', 'none');
		}
	});
	$('#business').change(function () {
		if ($(this).val() == 'on') {
			$('#wrapper_business').css('display', 'block');
			$('#wrapper_engineer').css('display', 'none');
		}
	});


	// Page one (engineer)
	$('#e_next_btn_1').click(function() {
		if(!isNextBtnClicked) {
			validate(1);
		} else {
			$("#business").attr("disabled", true);
			$('#basic_info').hide();
			$('#interests_info').show();
		}
		page1.done(function(data){
			if(data.success) isNextBtnClicked = true;
		});
	});

	// Page one (business)
	$('#b_next_btn').click(function() {
		if(!isNextBtnClicked) {
			validate(4, "business");
		} else {
			$("#engineer").attr("disabled", true);
			$('#business_basic_info').hide();
			$('#business_job_info').show();
		}
		page4.done(function(data){
			if(data.success) isNextBtnClicked = true;
		});
	});


	// Page two
	$('#e_next_btn_2').click(function() {
		if(!isNextBtn2Clicked) {
			validate(2);
		} else {
			$('#interests_info').hide();
			$('#job_info').show();
		}
		page2.done(function(data){
			if(data.success) isNextBtn2Clicked = true;
		});
	});

	if ($('#basic_info').is(':visible')) {
		$("input").keypress(function (e) {
			var keycode = e.charCode || e.keyCode;
			//Enter key's keycode
			if (keycode  == 13) {
				$('#e_next_btn_1').trigger('click');
			}
		});
	}

	$(".field").focus(function() {
		isNextBtnClicked = false;
		resetField(this);
	});

	$(".fieldSelect").change(function() {
		isNextBtnClicked = false;
		resetField(this);
	});

	// Page two
	$('#b_back_btn').click(function() {
		$("#engineer").attr("disabled", false);
		$('#business_basic_info').show();
		$('#business_job_info').hide();
	});

	$('#e_back_btn_1').click(function() {
		$("#business").attr("disabled", false);
		$('#basic_info').show();
		$('#interests_info').hide();
	});

	// Page three
	$('#e_back_btn_2').click(function() {
		$('#basic_info').hide();
		$('#interests_info').show();
		$('#job_info').hide();
	});

	$(".pi_checks").change(function() {
		resetInterestsErrors('product_interests');
	});

	$(".ii_checks").change(function() {
		resetInterestsErrors('industry_interests');
	});

	$("#accept_terms").change(function() {
		var errorContainers = $(".error-page-three");
		$.each( errorContainers, function( k, div ) {
			if(div.className.indexOf("accept_terms") != -1) {
				$(div).fadeOut(300, function(){
					$(this).html("");
				});
				return false;
			}
		});
	});

	$("#accept_terms_2").change(function() {
		var errorContainers = $(".business-error-page-two");
		$.each( errorContainers, function( k, div ) {
			if(div.className.indexOf("accept_terms_2") != -1) {
				$(div).fadeOut(300, function(){
					$(this).html("");
				});
				return false;
			}
		});
	});

	$("#submit_register").click(function() {
		validate(3);
		return false;
	});

	$("#submit_business_register").click(function() {
		validate(5, "business");
		return false;
	});

	function resetField(el) {
		var errorContainers = $(".error-below-field");
		var fieldId = el.id;

		$.each( errorContainers, function( k, div ) {
			if(div.className.indexOf(fieldId) != -1) {
				$(div).fadeOut(300, function(){
					$(this).html("");
				});
				return false;
			}
		});
	}

	function resetInterestsErrors(name) {
		var errorContainers = $("#wrapper_engineer .error-page-two");
		$.each( errorContainers, function( k, div ) {
			if(div.className.indexOf(name) != -1) {
				$(div).fadeOut(300, function(){
					$(this).html("");
				});
				return false;
			}
		});
	}

	function validate(page, type) {
		var form, parts;

		type = (typeof type == "undefined") ? "engineer" : type; 
		form = (type == "engineer") ?  $('#register_form').serialize() : $('#register_business_form').serialize();

		// console.log(form);
		parts = form.split('&');

		var errorContainers = "";
		var fieldsToRemove = [];
		
		var tempStr ="";

		switch(page) {
			case 1:
				errorContainers = $("#wrapper_engineer .error-below-field");

				for (var i = 0; i < parts.length; i++) {

					if(!(parts[i].indexOf("industry_interests") != -1) && 
						!(parts[i].indexOf("product_interests=") != -1) &&
						!(parts[i].indexOf("accept_terms=") != -1) &&
						!(parts[i].indexOf("subscribe_newsletter=") != -1)
					){
						// Change ACT to action
						if((parts[i].indexOf("ACT") != -1)) {
							parts[i] = parts[i].replace(/ACT/g, 'action');
						}

						tempStr += parts[i]+"&";
					}


				}

				form = tempStr.slice(0, -1);

				page1 = $.ajax({
					dataType: "json",
					type: "POST",
					url: "{{ajax_url}}",
					data: form+"&p=1",
					beforeSend: function() {
						$('.loading').fadeIn();
					},

					success: function(data) {

						if(data.success) {
							$("#business").attr("disabled", true);
							$('#basic_info').hide();
							$('#interests_info').show();
						} else {
							$.each( data, function( key, val ) {
								$.each( errorContainers, function( k, div ) {
									if(div.className.indexOf(key) != -1) {
										htmlstr = "<span class='alert-box alert'>";
										htmlstr += val;
										htmlstr += "</span>";
										$(div).fadeIn(300, function(){
											$(this).html(htmlstr);
										});
									}
								});
							});
						}

					},

					error: function(xhr, status, err) {
						console.log("xhr: ", xhr);
						console.log("status: ", status);
						console.log("err: ", err);
					},

					complete: function() {
						
						$('.loading').fadeOut();
					}
				});
				break;

			case 2:

				errorContainers = $("#wrapper_engineer .error-page-two");

				for (var i = 0; i < parts.length; i++) {
					if(!(parts[i].indexOf("firstname=") != -1) && !(parts[i].indexOf("lastname=") != -1) &&
						!(parts[i].indexOf("email=") != -1) && !(parts[i].indexOf("password=") != -1) &&
						!(parts[i].indexOf("password_confirm=") != -1) && !(parts[i].indexOf("company=") != -1) && 
						!(parts[i].indexOf("phone_number=") != -1) && !(parts[i].indexOf("country=") != -1) &&
						!(parts[i].indexOf("subscribe_newsletter=") != -1)
					){
						// Change ACT to action
						if((parts[i].indexOf("ACT") != -1)) {
							parts[i] = parts[i].replace(/ACT/g, 'action');
						}
						tempStr += parts[i]+"&";
					}
				};
				
				form = tempStr.slice(0, -1);

				page2 = $.ajax({
					dataType: "json",
					type: "POST",
					url: "{{ajax_url}}",
					data: form+"&p=2",
					beforeSend: function() {
						$('.loading').fadeIn();
					},
					success: function(data) {
						if(data.success) {
							$('#interests_info').hide();
							$('#job_info').show();
						} else {
							$('.loading').fadeOut();
							$.each( data, function( key, val ) {
								$.each( errorContainers, function( k, div ) {
									if(div.className.indexOf(key) != -1) {
										htmlstr = "<span class='alert-box alert'>";
										htmlstr += val;
										htmlstr += "</span>";
										$(div).fadeIn(300, function(){
											$(this).html(htmlstr);
										});
									}
								});
							});
						}
					},
					complete: function() {
						$('.loading').fadeOut();
					}
				});

				break;

			case 3:
				errorContainers = $("#wrapper_engineer .error-page-three");

				for (var i = 0; i < parts.length; i++) {
					if(!(parts[i].indexOf("password=") != -1) &&
						!(parts[i].indexOf("password_confirm=") != -1) 
					){
						if((parts[i].indexOf("ACT") != -1)) {
							parts[i] = parts[i].replace(/ACT/g, 'action');
						}
						tempStr += parts[i]+"&";
					}
				}
				
				form = tempStr.slice(0, -1);

				$.ajax({
					dataType: "json",
					type: "POST",
					url: "{{ajax_url}}",
					data: form+"&p=3",
					beforeSend: function() {
						$('.loading').fadeIn();
					},
					success: function(data) {
						if(data.success) {
							$('#register_form').submit();
						} else {
							$('.loading').fadeOut();
							$.each( data, function( key, val ) {
								$.each( errorContainers, function( k, div ) {
									if(div.className.indexOf(key) != -1) {
										htmlstr = "<span class='alert-box alert'>";
										htmlstr += val;
										htmlstr += "</span>";
										$(div).fadeIn(300, function(){
											$(this).html(htmlstr);
										});
									}
								});
							});
						}
					}
				});

				break;
			// Business 1st page
			case 4:
				errorContainers = $("#wrapper_business .error-below-field");

				for (var i = 0; i < parts.length; i++) {
					if(!(parts[i].indexOf("industry_interests") != -1) && 
						!(parts[i].indexOf("product_interests=") != -1) &&
						!(parts[i].indexOf("accept_terms=") != -1) &&
						!(parts[i].indexOf("subscribe_newsletter=") != -1)
					){

						// Change ACT to action
						if((parts[i].indexOf("ACT") != -1)) {
							parts[i] = parts[i].replace(/ACT/g, 'action');
						}

						tempStr += parts[i]+"&";
					}
				}

				form = tempStr.slice(0, -1);

				page4 = $.ajax({
					dataType: "json",
					type: "POST",
					url: "{{ajax_url}}",
					data: form+"&p=4",
					beforeSend: function() {
						$('.loading').fadeIn();
					},
					success: function(data) {
						if(data.success) {
							$("#engineer").attr("disabled", true);
							$('#business_basic_info').hide();
							$('#business_job_info').show();
						} else {
							$.each( data, function( key, val ) {
								$.each( errorContainers, function( k, div ) {
									if(div.className.indexOf(key) != -1) {
										htmlstr = "<span class='alert-box alert'>";
										htmlstr += val;
										htmlstr += "</span>";
										$(div).fadeIn(300, function(){
											$(this).html(htmlstr);
										});
									}
								});
							});
						}

					},
					complete: function() {
						$('.loading').fadeOut();
					}
				});
				break;
			// Business second page
			case 5:
				errorContainers = $("#wrapper_business .business-error-page-two");

				for (var i = 0; i < parts.length; i++) {
					if(!(parts[i].indexOf("password=") != -1) &&
						!(parts[i].indexOf("password_confirm=") != -1) 
					){
						if((parts[i].indexOf("ACT") != -1)) {
							parts[i] = parts[i].replace(/ACT/g, 'action');
						}
						// Change ACT to action
						tempStr += parts[i]+"&";
					}
				};
				
				form = tempStr.slice(0, -1);

				$.ajax({
					dataType: "json",
					type: "POST",
					url: "{{ajax_url}}",
					data: form+"&p=5",
					beforeSend: function() {
						$('.loading').fadeIn();
					},
					success: function(data) {
						if(data.success) {
							$('#register_business_form').submit();
						} else {
							$('.loading').fadeOut();
							$.each( data, function( key, val ) {
								$.each( errorContainers, function( k, div ) {
									if(div.className.indexOf(key) != -1) {
										htmlstr = "<span class='alert-box alert'>";
										htmlstr += val;
										htmlstr += "</span>";
										$(div).fadeIn(300, function(){
											$(this).html(htmlstr);
										});
									}
								});
							});
						}
					}
				});

				break;
		}
	}

	// Upload profile picture
	$("#choose_avatar").click(function(){
		$("#avatar").click();
		return false;
	});

	$("#choose_avatar_business").click(function(){
		$("#avatar_business").click();
		return false;
	});

	var options = {
		thumbBox: '.thumbBox',
		spinner: '.spinner',
		imgSrc: ''
	}
	var cropper = $('.imageBox').cropbox(options);
	var cropper_business = $('.imageBox_business').cropbox(options);

	$('#avatar').on('change', function(){
		var reader = new FileReader();
		reader.onload = function(e) {
			options.imgSrc = e.target.result;
			cropper = $('.imageBox').cropbox(options);
			$('#image_src').val(options.imgSrc);
			$('.cropped').html('<img src="'+options.imgSrc+'" width="210" height="158">');
		}
		var avatar_info = this.files[0];
		$('#image_name').val(avatar_info.name);
		// console.log("avatar_info: ", avatar_info);
		reader.readAsDataURL(this.files[0]);
		
		this.files = [];
	});

	$('#avatar_business').on('change', function(){
		var reader = new FileReader();
		reader.onload = function(e) {
			options.imgSrc = e.target.result;
			cropper_business = $('.imageBox_business').cropbox(options);
			$('#image_src_business').val(options.imgSrc);
			$('.cropped_business').html('<img src="'+options.imgSrc+'" width="210" height="158">');
		}
		var avatar_info = this.files[0];
		$('#image_name_business').val(avatar_info.name);
		// console.log("avatar_info: ", avatar_info);
		reader.readAsDataURL(this.files[0]);
		
		this.files = [];
	});

	$('#engineerImageBox').on('click', function(){
		var attrStyle = $(this).attr("style");

		if(typeof attrStyle == "undefined") {
			$("#avatar").click();
		}
	});

	$('#businessImageBox').on('click', function(){
		var attrStyle = $(this).attr("style");

		if(typeof attrStyle == "undefined") {
			$("#avatar_business").click();
		}
	});

	$('#btnCrop').on('click', function(){
		var img = cropper.getAvatar();
		$('#image_src').val(img);
		$('.cropped').html('<img src="'+img+'">');
	});
	$('#btnCrop_business').on('click', function(){
		var img = cropper_business.getAvatar();
		$('#image_src_business').val(img);
		$('.cropped_business').html('<img src="'+img+'">');
	});
	$('#btnZoomIn').on('click', function(){
		cropper.zoomIn();
	});
	$('#btnZoomIn_business').on('click', function(){
		cropper_business.zoomIn();
	});
	$('#btnZoomOut').on('click', function(){
		cropper.zoomOut();
	});
	$('#btnZoomOut_business').on('click', function(){
		cropper_business.zoomOut();
	});
});