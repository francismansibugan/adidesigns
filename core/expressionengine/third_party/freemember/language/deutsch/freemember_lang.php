<?php

$lang = array(
    'freemember_module_name' => 'Freemember',
    'freemember_module_description' => 'Free your member templates!',

    'captcha' => 'Captcha',
    'accept_terms' => 'Terms of Service',
    'current_password' => 'Current Password',
    'invalid_email' => 'Invalid email address',
    'invalid_username' => 'Invalid username',
    'fm_invalid_selection' => 'The %s field contains an invalid selection.',
    'no_product_interest' => 'Bitte wählen Sie mindestens eine (1) Produktinteresse aus.',
    'no_industry_interest' => 'Bitte wählen Sie mindestens eine (1) Industrieinteresse aus.',
    'no_business_name' => 'Das Unternehmen Feld ist Bedingung.',
    'email_required'=>'Email is required.'
);
