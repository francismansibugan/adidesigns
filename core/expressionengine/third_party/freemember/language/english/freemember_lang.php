<?php

$lang = array(
    'freemember_module_name' => 'Freemember',
    'freemember_module_description' => 'Free your member templates!',

    'captcha' => 'Captcha',
    'accept_terms' => 'Terms of Service',
    'current_password' => 'Current Password',
    'invalid_email' => 'Invalid email address',
    'invalid_username' => 'Invalid username',
    'fm_invalid_selection' => 'The %s field contains an invalid selection.',
    'no_product_interest' => 'Please select at least one (1) Product Interest.',
    'no_industry_interest' => 'Please select at least one (1) Industry Interest.',
    'no_business_name' => 'Business name is required.',
    'email_required'=>'Email is required.'
);
