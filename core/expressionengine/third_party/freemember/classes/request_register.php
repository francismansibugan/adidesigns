<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(PATH_THIRD.'ajaxrequest/libraries/request_class.php');
require_once(PATH_THIRD.'ajaxrequest/libraries/request_interface.php');

class Request_register extends Request implements RequestInterface {
	private $classname = "register";
	private $module = "freemember";

	/**
	 * Returns the ajax response
	 * @return array 
	 */
	public function response() {

		$csrf_token	= ee()->csrf->get_user_token();

		ee()->load->add_package_path(PATH_THIRD.'freemember/');
		ee()->load->library('freemember_lib');
		ee()->freemember->register();

		// Get the actual action ID
		$action_id = (int) ee()->db->where(array('class' => 'Freemember', 'method' => 'act_register'))->get('actions')->row('action_id'); 

		if( ($_POST['csrf_token'] == $csrf_token) && 
			is_numeric($_POST['action']) && 
			is_numeric($_POST['p']) && 
			($_POST['action'] == $action_id) ) {

			// Get errors, if there's any.
			$err = ee()->form_validation->error_array();

			// Page 1 (engineer)
			if($_POST['p'] == 1) {
				foreach ($err as $k => $v) {
					if($k == 'product_interests' || $k == 'industry_interests' || $k == 'accept_terms' || $k == 'screen_name' || $k == 'job_function') {
						unset($err[$k]);
					}
				}
			}

			// Page 2 (engineer)
			if($_POST['p'] == 2) {
				foreach ($err as $k => $v) {
					if($k != 'product_interests' && $k != 'industry_interests') {
						unset($err[$k]);
					} 
				}
			}

			// Page 3 (engineer)
			if($_POST['p'] == 3) {
				foreach ($err as $k => $v) {
					if($k != 'job_function' && $k != 'accept_terms') {
						unset($err[$k]);
					} 
				}
			}

			// Page 4 (business)
			if($_POST['p'] == 4) {
				$exclude = array(
					'product_interests',
					'industry_interests',
					'accept_terms',
					'screen_name',
					'job_function',
					'lastname',
					'country',
					'company'
					);
				foreach ($err as $k => $v) {
					if(in_array($k, $exclude)) {
						unset($err[$k]);
					}
				}
			}

			// Page 5 (business)
			if($_POST['p'] == 5) {
				foreach ($err as $k => $v) {
					if($k != 'accept_terms') {
						unset($err[$k]);
					}
				}
			}

			if(!empty($err)) {
				$err['success'] = false;
				return $err;
			} else {
				$data['success'] = true;
				return $data;
			}
		}
	}


	/**
	 * Returns the javascript code
	 * Note: You can pass 'ACT' value but change the name to 'action'
	 */
	public function setjs() {
		$tag_maps = array(
			'ajax_url' => $this->ajax_url($this->classname, $this->module)
		);

		// placeholder
		$script	= '';

		// load script
		$js_path = PATH_THIRD . '/freemember/javascript/register.js';

		return parse_content($tag_maps, $js_path);
	}

}