<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(PATH_THIRD.'ajaxrequest/libraries/request_class.php');
require_once(PATH_THIRD.'ajaxrequest/libraries/request_interface.php');

class Request_login extends Request implements RequestInterface {
	private $classname = "login";
	private $module = "freemember";

	/**
	 * Returns the ajax response
	 * @return array 
	 */
	public function response() {

		$csrf_token	= ee()->csrf->get_user_token();
		ee()->load->add_package_path(PATH_THIRD.'freemember/');
		ee()->load->library('freemember_lib');
		
		$action_id = (int) ee()->db->where(array('class' => 'Freemember', 'method' => 'act_login'))->get('actions')->row('action_id'); 

		if( ($_POST['csrf_token'] == $csrf_token) && 
			is_numeric($_POST['action']) && 
			($_POST['action'] == $action_id) ) {

			$err = ee()->freemember->login();

			if(!empty($err)) {
				$err['success'] = false;
				return $err;
			} else {
				ee()->session->set_flashdata('set_status', TRUE);
				$return_url = ee()->input->get_post('return_url') ? : $this->history(0);
				$return_url = ee()->functions->create_url($return_url);
				if (isset($_POST['_params']) && ee()->freemember->form_param('secure_return') == 'yes')
				{
					$return_url = str_replace('http://', 'https://', $return_url);
				}
				$data['success'] = true;
				$data['url'] = $return_url;
				return $data;
			}
		}
	}
	
	protected function history($id)
	{
		$tracker = ee()->session->tracker;

		if (isset($tracker[$id]))
		{
			if ($tracker[$id] === 'index')
			{
				return '/';
			}

			return $tracker[$id];
		}
	}

	/**
	 * Returns the javascript code
	 * Note: You can pass 'ACT' value but change the name to 'action'
	 */
	public function setjs() {
		$tag_maps = array(
			'ajax_url' => $this->ajax_url($this->classname, $this->module)
		);

		// placeholder
		$script	= '';

		// load script
		$js_path = PATH_THIRD . '/freemember/javascript/login.js';

		return parse_content($tag_maps, $js_path);
	}
}
