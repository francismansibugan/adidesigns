<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require_once(PATH_THIRD.'ti_designs/libraries/ganon.php');

class Ti_designs
{
	public $return_data = '';
	
	public function __construct()
	{
		// load library
		ee()->load->library();

		// load models
		ee()->load->model('ti_designs_designs_model', 'designs');
	}
	
	public function designs()
	{
		$return 	= '';
		$categories = ee()->input->get_post('cat') ? explode(',', ee()->input->get_post('cat')) : array();
		$per_page 	= ee()->TMPL->fetch_param('limit', 0);
		$sort 		= ee()->TMPL->fetch_param('sort', 'desc');
		$order_by	= ee()->TMPL->fetch_param('orderby', 'id');
		$status		= ee()->TMPL->fetch_param('status');
		$tagdata	= ee()->TMPL->tagdata;
		
		foreach ($categories as $category)
		{
			ee()->db->or_like('category', $category);
		}

		if($status)
		{
			ee()->db->where(array('status' => $status));
		}
		ee()->db->order_by($order_by, $sort);

		if(in_array('Other', $categories))
		{
			ee()->db->or_where('category IS NULL', NULL, FALSE);
		}
		
		$results = ee()->designs->get_all();

		if($status == "f") {
			// init vars
			$ch = null;
			$cookie_file = null;
			$curl_errors = '';

			foreach ($results as $k => $v) {
				if ($v['image_url'] == '') {
					$html = $this->__get_dom_elements($v['url'], $ch, null, $curl_errors);

					$dom = str_get_dom($html);
					$img = $dom('#image_1 img', 0);

					$v['image_url'] = is_object($img) ? str_replace('tn_', 'med_', $img->src) : '';

					ee()->designs->save_image_url($v);
				}	
			}
		}

		if(count($results) == 0 )
		{
			return ee()->TMPL->no_results();
		}

		// Start up pagination
		ee()->load->library('pagination');
        
		$pagination = ee()->pagination->create();
        
		$tagdata = $pagination->prepare($tagdata);
		
        // Disable pagination if the limit parameter isn't set
		if (empty($per_page))
		{
			$pagination->paginate = FALSE;
		}

		if ($pagination->paginate)
		{
            $pagination->build(count($results), $per_page);
			$results = array_slice($results, $pagination->offset, $pagination->per_page);
		}
		else
		{
			$results = array_slice($results, 0, $per_page);
		}
		
		foreach($results as $result)
		{
			foreach(ee()->TMPL->var_pair as $pair => $value)
			{
				if(isset($result[$pair]))
				{
					$col = explode(',', $result[$pair]);
					$arr = array();
					$count = 0;
					
					foreach($col as $val)
					{
						$arr[] = array(
							$pair => $val,
							'count' => ++$count
						);
						
						$result[$pair] = $arr;
					}
				}
			}
			
			$data[] = $result;
		}
		
		$return = ee()->TMPL->parse_variables($tagdata, array_values($data));
		
		if ($pagination->paginate === TRUE)
		{
			$return = $pagination->render($return);
		}
		
		return $return;
	}

	public function categories()
	{
		$return 	= '';
		$categories = array();
		
		$results = ee()->designs->get_categories();
		
		foreach($results as $result)
		{
			$categories[] = array(
				'category' => $result,
				'short_name' => strtolower(clean_string($result))
			);
		}
		
		foreach($categories as $category)
		{
			$return .=  ee()->functions->var_swap(ee()->TMPL->tagdata, $category);
		}
		
		return $return;
	}

	private function __get_dom_elements($url, &$ch, $options=null, &$errors=null) {
		// init curl
		if (!$ch) {
			$ch	= curl_init();

			// set cookie file
			$cookie_file	= tempnam ("/tmp", 'curl-cookie.' . md5(time()));

			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
		}

		curl_setopt($ch, CURLOPT_URL, $url);
		if (is_array($options)) {
			curl_setopt_array($ch, $options);
		}

		ob_start();
		curl_exec($ch); // <--- json
		$results	= ob_get_clean();

		// has errors
		if (curl_errno($ch)) {
			$errors	= curl_error($ch);
		}

		return $results;
	}
}

/* End of file mod.ti_designs.php */
/* Location: /system/expressionengine/third_party/ti_designs/mod.ti_designs.php */
