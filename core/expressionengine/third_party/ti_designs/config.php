<?php
if (!defined("BASEPATH"))
	die("No direct script access allowed");

$config['name'] = "";
$config['version'] = "1.0";

// CONSTANTS
if (!defined("TI_DESIGNS_VERSION"))
{
	define('TI_DESIGNS_VERSION', $config['version']);
}
if (!defined("TI_DESIGNS_NAME"))
{
	define('TI_DESIGNS_NAME', $config['name']);
}
