<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

$lang = array(
	// Required for ADD-ONS > MODULES page
	'ti_designs_module_name' 			=> 'TI Designs',
	'ti_designs_module_description' 	=> 'TI Designs Module',
	
	/* ---------------------------------------------------- */
	
	// table columns
	'ti_designs_model_number' 		=> 'Model Number',
	'ti_designs_title' 				=> 'Title',
	'ti_designs_description' 		=> 'Description',
	'ti_designs_url' 				=> 'URL',
	'ti_designs_category' 			=> 'Category',
	'ti_designs_pcb' 				=> 'PCB',
	'ti_designs_schematic' 			=> 'Schematic',
	'ti_designs_test_data' 			=> 'Test Data',
	'ti_designs_design_file' 		=> 'Design File',
	'ti_designs_bom' 				=> 'BOM',
	'ti_designs_image_url' 		    => 'Image',
	'ti_designs_status' 			=> 'Status',
	'ti_designs_preference' 		=> 'Preference',
	'ti_designs_setting' 			=> 'Setting',
	
	// labels
	'ti_designs_create'				=> 'Create New Design',
	'ti_designs_import'				=> 'Import',
	'ti_designs_upload'				=> 'Import from CSV',
	'ti_designs_designs'			=> 'Designs',
	'ti_designs_filter'				=> 'Filter Designs',
	'ti_designs_prefix'				=> 'Prefix',
	'ti_designs_url_code'			=> 'URL Code',
	'ti_designs_o'					=> 'Open',
	'ti_designs_f'					=> 'Featured',
	'ti_designs_set_o'				=> 'Set Status to Open',
	'ti_designs_set_f'				=> 'Set Status to Featured',
	'ti_designs_destroy' 			=> 'Delete Selected',
	'ti_designs_add_pcb' 			=> 'Add PCB',
	'ti_designs_add_schematic' 		=> 'Add Schematic',
	'ti_designs_add_test_data' 		=> 'Add Test Data',
	'ti_designs_add_design_file' 	=> 'Add Design File',
	'ti_designs_add_bom' 			=> 'Add BOM',
	
	// messages
	'ti_designs_no_result'			=> 'Nothing to see here ..',
	'ti_designs_empty'				=> 'Title or Model Number cannot be empty!',
	'ti_designs_upload_error'		=> 'There was an error in uploading your file. Please make sure you have the right CSV and try again.',
	'ti_designs_prefs_error'		=> 'URL Code cannot be empty!',
	'ti_designs_selection_required'	=> 'At least one selection is required to submit',
	
	/* ---------------------------------------------------- */
	
	'' => ''
);

/* End of file lang.ti_designs.php */
/* Location: /system/expressionengine/third_party/ti_designs/language/english/lang.ti_designs.php */