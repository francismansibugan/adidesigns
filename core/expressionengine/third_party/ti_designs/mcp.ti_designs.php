<?php
set_time_limit(0);

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require PATH_THIRD . "ti_designs/libraries/Ti_designs_model.php";

define('TI_DESIGNS_URL', BASE . AMP . 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=ti_designs');
define('TI_DESIGNS_PATH', 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=ti_designs');

class Ti_designs_mcp
{
	public $data = array();

	// Constructor
	public function __construct()
	{
		// load libraries
		ee()->load->library(array(
			'ti_designs_lib',
			'ti_designs_model',
			'form_validation',
			'table',
			'javascript'
		));

		// load helpers
		ee()->load->helper('eeweb');

		// load models
		ee()->load->model('ti_designs_designs_model', 'designs');

		// JS
		ee()->cp->load_package_js('ti_designs');

		// Add nav globally
		ee()->cp->set_right_nav(array('ti_designs_module_name' => TI_DESIGNS_URL));

		// MSM
		$this->site_id = ee()->config->item('site_id');

		// Theme URL
		define('TI_DESIGNS_THEME_URL', $this->_theme_url());
	}

	/**
	 * The module's Control Panel default controller
	 */
	public function index()
	{
		$offset 	= ee()->input->get_post('per_page');
		$keywords 	= ee()->input->get_post('keywords');
		$row_limit 	= 20;

		$defaults = array('sort' => array('id' => 'desc'));
		$params = array(
			'limit' => $row_limit,
			'keywords' => $keywords
		);

		// js
		ee()->javascript->set_global(array(
			'ti_designs.run_script' => 'setup_index',
			'lang.selection_required' => lang('ti_designs_selection_required')
		));

		$this->data 					= array_merge($this->data, ee()->table->datasource('_source', $defaults, $params));
		$this->data['prefs_errors'] 	= (ee()->session->flashdata('prefs_validate') == 'yes') ? TRUE : FALSE;
		$this->data['upload_errors'] 	= (ee()->session->flashdata('upload_validate') == 'yes') ? TRUE : FALSE;
		$this->data['url_code'] 		= ee()->config->item('ti_designs_url_code');
		$this->data['prefix'] 			= ee()->config->item('ti_designs_prefix');
		$this->data['data'] 			= ee()->designs->get_all();
		$this->data['form_options']		= array(
			'f' => lang('ti_designs_set_f'),
			'o' => lang('ti_designs_set_o'),
			'null' => '------',
			'delete' => lang('ti_designs_destroy')
		);

		// show the view
		$this->_title('ti_designs_module_name');
		return $this->_view('cp/index');
	}

	/**
	 * Create/Edit Design
	 */
	public function edit_design()
	{
		$default = array(
			'id' 			=> NULL,
			'model_number' 	=> NULL,
			'title' 		=> NULL,
			'description' 	=> NULL,
			'url' 			=> NULL,
			'image_url'		=> NULL,
			'pcb' 			=> NULL,
			'schematic' 	=> NULL,
			'test_data' 	=> NULL,
			'design_file' 	=> NULL,
			'bom' 			=> NULL,
			'category' 		=> NULL,
			'status' 		=> NULL
		);
		$category_options 		= array();
		$design_id 				= ee()->input->get_post('design_id');
		$design 				= ee()->designs->get($design_id);
		$design 				= array_merge($default, $design);
		$design['category'] 	= explode(', ', $design['category']);
		$design['pcb'] 			= explode(',', $design['pcb']);
		$design['schematic'] 	= explode(',', $design['schematic']);
		$design['test_data'] 	= explode(',', $design['test_data']);
		$design['design_file'] 	= explode(',', $design['design_file']);
		$design['bom'] 			= explode(',', $design['bom']);
		
		foreach(ee()->designs->get_categories() as $category)
		{
			$category_options[$category] = $category;
		}

		$this->data['design'] 			= $design;
		$this->data['status_options'] 	= array(
			'o' => lang('ti_designs_o'),
			'f' => lang('ti_designs_f')
		);
		$this->data['category_options']	= $category_options;

		// show the view
		ee()->output->send_ajax_response(array('success' => $this->_view('cp/design_edit')));
	}

	/**
	 * Save Design
	 */
	public function save_design()
	{
		// empty name?
		if (!ee()->input->get_post('title') || !ee()->input->get_post('model_number'))
		{
			ee()->output->send_ajax_response(array('error' => lang('ti_designs_empty')));
		}

		$redirect	= TI_DESIGNS_URL;
		$design_id	= ee()->input->get_post('design_id');
		$data		= $_POST;
		$url_code 	= NULL;
		$prefix		= NULL;

		$data['url']			= ee()->input->get_post('url') ? ee()->ti_designs_lib->_add_code($data['url'], $url_code, $prefix) : NULL;
		// comma delimited
		$data['category']		= ee()->input->get_post('category') ? implode(', ', $data['category']) : NULL;
		$data['pcb'] 			= ee()->input->get_post('pcb') ? implode(',', ee()->ti_designs_lib->_add_code($data['pcb'], $url_code, $prefix)) : NULL;
		$data['schematic'] 		= ee()->input->get_post('schematic') ? implode(',', ee()->ti_designs_lib->_add_code($data['schematic'], $url_code, $prefix)) : NULL;
		$data['test_data'] 		= ee()->input->get_post('test_data') ? implode(',', ee()->ti_designs_lib->_add_code($data['test_data'], $url_code, $prefix)) : NULL;
		$data['design_file'] 	= ee()->input->get_post('design_file') ? implode(',', ee()->ti_designs_lib->_add_code($data['design_file'], $url_code, $prefix)) : NULL;
		$data['bom']			= ee()->input->get_post('bom') ? implode(',', ee()->ti_designs_lib->_add_code($data['bom'], $url_code, $prefix)) : NULL;

		// unset
		unset($data['design_id']);
		unset($data['submit']);

		if ($design_id == NULL)
		{
			ee()->designs->insert($data);
		}
		else
		{
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			ee()->designs->update($design_id, $data);
		}

		ee()->session->set_flashdata('message_success', "Design Saved");
		ee()->output->send_ajax_response(array(
			'success' => TRUE,
			'location' => $redirect,
			'force_refresh' => TRUE
		));
	}

	/**
	 * Modify Status / Delete Designs
	 */
	public function modify_designs()
	{
		$redirect 	= TI_DESIGNS_URL;
		$design_ids	= ee()->input->get_post('toggle');
		$action		= ee()->input->get_post('action');
		
		switch ($action)
		{
			case 'f':
			case 'o':
				$data = array(
					'status' => $action,
					'updated_at' => date('Y-m-d H:i:s')
				);
				
				ee()->designs->update_many($design_ids, $data);
				ee()->session->set_flashdata('message_success', "Status Changed");
				break;
			case 'delete':
				// delete them
				ee()->designs->delete_many($design_ids);
				ee()->session->set_flashdata('message_success', "Design Removed");
				break;
		}
		
		ee()->functions->redirect($redirect);
	}
	
	/**
	 * Update Prefs
	 */
	public function update_preferences()
	{
		$redirect = TI_DESIGNS_URL;
		$url_code = ee()->input->get_post('url_code');
		$old_prefix = ee()->config->item('ti_designs_prefix');
		$prefix = ee()->input->get_post('prefix');

		if(empty($url_code))
		{
			ee()->session->set_flashdata('prefs_validate', 'yes');
			ee()->functions->redirect($redirect);
		}

		if(empty($prefix))
		{
			ee()->session->set_flashdata('prefs_validate', 'yes');
			ee()->functions->redirect($redirect);
		}
		
		ee()->config->update_site_prefs(array(
			'ti_designs_prefix' => $prefix,
			'ti_designs_url_code' => $url_code
		));
		
		ee()->ti_designs_lib->update_urls($url_code, $prefix, $old_prefix);
		
		ee()->session->set_flashdata('message_success', "Preferences Updated");
		ee()->functions->redirect($redirect);
	}
	
	/**
	 * Import CSV
	 */
	public function import_csv()
	{
		$redirect 	= TI_DESIGNS_URL;
		$csv_type	= ee()->input->get_post('csv_type');
		$tmpdir 	= substr(PATH_THEMES, 0, strpos(PATH_THEMES, 'themes/')) . 'uploads/csv/';
		$file 		= $tmpdir . time() . $_FILES['file']['name'];

		if (!move_uploaded_file($_FILES['file']['tmp_name'], $file))
		{
			ee()->session->set_flashdata('upload_validate', 'yes');
			ee()->functions->redirect($redirect);
		}

		ee()->ti_designs_lib->process_csv($file, $csv_type);

		ee()->session->set_flashdata('message_success', "Imported data successfully!");
		ee()->functions->redirect($redirect);
	}

	/**
	 * Designs table source
	 */
	public function _source($state, $params)
	{
		$columns = array(
			'_expand'		=> array(
				'header' => array('data' => '+/-', 'class' => 'expand'),
				'sort'	 => FALSE
		));
		$fields = ee()->db->list_fields('ti_designs_designs');
		
		foreach ($fields as $field)
		{
			if($field == 'description' || $field == 'id' || $field == 'created_at' || $field == 'updated_at')
				continue;
			
			$columns[$field] = array('header' => ee()->lang->line('ti_designs_' . $field));
		}
		$columns['_check'] = array(
			'header' => form_checkbox('toggle_comments', 'true', FALSE, 'class="toggle_comments"'),
			'sort' => FALSE
		);

		ee()->table->set_columns($columns);
		ee()->table->set_base_url(TI_DESIGNS_PATH);

		ee()->db->where(get_sc($fields, $params['keywords']));

		foreach ($state['sort'] as $column => $direction)
		{
			ee()->db->order_by($column, $direction);
		}

		ee()->db->limit($params['limit'], $state['offset']);

		$designs = ee()->designs->get_all();
		$total_count = (count($designs) < $params['limit']) ? count($designs) : ee()->designs->count_all();

		$rows = array();
		while ($d = array_shift($designs))
		{
			$d['_expand'] = array(
				'data' => '<img src="'.ee()->cp->cp_theme_url.'images/field_collapse.png" alt="'.lang('expand').'" />',
				'class' => 'expand'
			);
			$d['title'] = '<a class="ti_designs_edit" href="' . TI_DESIGNS_URL . AMP . 'design_id=' . $d['id'] . AMP . 'method=edit_design">' . $d['title'] . '</a>';
			$d['description'] = array(
				'data' => '<div>'.$d['description'].'</div>',
				'colspan' => 13
			);
			$d['status'] = lang('ti_designs_' . $d['status']);
			$d['_check'] = form_checkbox(
				'toggle[]', $d['id'], FALSE, 'class="ti_designs_toggle"'
			);

			$rows[] = (array)$d;
		}

		return array(
			'rows' => $rows,
			'no_results' => lang('ti_designs_no_result'),
			'keywords' => $params['keywords'],
			'pagination' => array(
				'per_page' => $params['limit'],
				'total_rows' => $total_count
			)
		);
	}

	/**
	 * Get the theme folder URL
	 */
	private function _theme_url()
	{
		if (!isset(ee()->session->cache['ti_designs']['theme_url']))
		{
			$theme_folder_url = ee()->config->item('theme_folder_url');
			if (substr($theme_folder_url, -1) != '/')
				$theme_folder_url .= '/';
			ee()->session->cache['ti_designs']['theme_url'] = $theme_folder_url . 'third_party/ti_designs/';
		}

		return ee()->session->cache['ti_designs']['theme_url'];
	}

	/**
	 * Set page titles and breadcrumb
	 */
	private function _title($title)
	{
		$this->data['title'] = lang($title);

		ee()->view->cp_page_title = lang($title);

		ee()->cp->set_breadcrumb(TI_DESIGNS_URL, "TI Designs");
	}

	/**
	 * Load the view through $this->data
	 */
	private function _view($view)
	{
		return ee()->load->view($view, $this->data, TRUE);
	}

}

/* End of file mod.ti_designs.php */
/* Location: /system/expressionengine/third_party/ti_designs/mcp.ti_designs.php
 * */
