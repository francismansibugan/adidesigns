!(function(e) {
	function t(t, o) {
		e.getJSON(t, function(data) {
			return (popup.find(".contents").html(data.success),
			void popup.dialog("option", "title", o).dialog("open"));
		});
	}
	
	var popup = e('<div id="popup" style="display:none;"><div class="contents"></div></div>');
	popup.dialog({
		width : 800,
		resizable : false,
		position : ["center", "center"],
		modal : true,
		draggable : !0,
		zIndex : 99999,
		autoOpen: false,
		open : function() {
			e("#cancel").click(function(e) {
				e.preventDefault(), popup.dialog("close");
			}), e('.add-field').on('click', function(t) {
				t.preventDefault();
				var which = this.id.split('-'), table = which[0], has_error = false;
				var tpl = '<tr><td style="width:90%"><input type="text" class="fullfield" name="' + table + '[]"></td><td style="width:8%"><a href="#" class="remove-field">Remove</a></td></tr>';
				e('#' + table + ' input[type="text"]').each(function() {
					if (!e(this).val()) has_error = true;
				});
				if(has_error) {
					e('#' + table + ' input[type="text"]').focus();
					return;
				}
				e(tpl).appendTo('#' + table);
			}), e('#edit_form').on('click', '.remove-field', function(t) {
				t.preventDefault();
				e(this).closest('tr').fadeOut(200, function() {
					e(this).remove();
				});
			}), popup.find("form").submit(function(t) {
				t.preventDefault();
				e.post(e(this).attr("action"), e(this).serialize(), function(data) {
					return data.error ?
					void     e(".error").text(data.error) : (e.ee_notice(data.success, {
						type : "success"
					}),
					void (data.force_refresh && (window.location = data.location.replace(/&amp;/g, '&'))));
				}, "json");
			});
		}
	}), e('a.ti_designs_create').on('click', function(o) {
		o.preventDefault();
		t(this.href.replace(/&amp;/g, '&'), e(this).html());
	});

	var Ti_designs_cp = {

		// INDEX PAGE

		table: null,
		data: null,
		html_data: null,

		detail_template: '<tr>' +
		'{{if $.isPlainObject(description)}}<td{{each description}}{{if $index != "data"}} ${$index}="${$value}" {{/if}}{{/each}}>{{html description.data}}{{else}}<td>{{html description}}{{/if}}</td>' +
		'</tr>',

		setup_index: function() {

			this.table = $('.mainTable');

			$.template('ti_designs_additional_row', this.detail_template);

			this.toggles();
			this.table_data();
			this.table_events();
			this.ajax_filter();
		},

		toggles: function() {
			this.table.toggle_all();

			$("#target").submit(function () {
				if ( ! $("input[class=ti_designs_toggle]", this).is(":checked")) {
					$.ee_notice(EE.lang.selection_required, {"type" : "error"});
					return false;
				}
			});
		},

		table_data: function() {
			this.data = this.table.table('get_current_data').rows,
				this.html_rows = this.table.find('tbody tr');
		},

		table_events: function() {
			var that = this,
				indicator = $('.searchIndicator');

			this.table.bind('tableupdate', function(evt, res) {
				that.html_rows = $(res.data.html_rows);
				that.data = res.data.rows;
			}).bind('tableload', function() {
				indicator.css('visibility', '');
			})
				.bind('tableupdate', function() {
					indicator.css('visibility', 'hidden');
				});

			this.table.delegate('.expand', 'click', function() {
				var el = $(this),
					current_row = el.closest('tr');

				if (el.data('expanded')) {
					that._collapse(el, current_row);
				} else {
					that._expand(el, current_row);
				}

				return false;
			});

			this.table.delegate('.ti_designs_edit', 'click', function(evt) {
				evt.preventDefault();
				t(this.href.replace(/&amp;/g, '&'), e(this).html());
			});
		},

		ajax_filter: function() {
			this.table.table('add_filter', $('#ti_designs_filter'));
		},

		_collapse: function(el, current_row) {
			// remove row
			current_row.next('tr').remove();

			// flag and image
			el.data('expanded', false);
			el.find('img').attr('src', EE.THEME_URL + "images/field_collapse.png");
		},

		_expand: function(el, current_row) {
			// parse row
			var index = Ti_designs_cp.html_rows.index(current_row),
				new_row = $.tmpl('ti_designs_additional_row', Ti_designs_cp.data[index]);

			// add it
			current_row.after(new_row);

			// flag and image
			el.data('expanded', true);
			el.find('img').attr('src', EE.THEME_URL + "images/field_expand.png");
		}
	};


	// run_script is set in the controller
	if (EE.ti_designs && EE.ti_designs.run_script) {
		setTimeout(function() {
			var script = EE.ti_designs.run_script;
			Ti_designs_cp[script]();
		}, 100);
	}
})(jQuery);