<?php
if (!defined('BASEPATH'))
	die('No direct script access allowed');

if(!class_exists('Ti_designs_model')) 
	require_once PATH_THIRD . "ti_designs/libraries/Ti_designs_model.php";

class Ti_designs_designs_ref_model extends Ti_designs_model
{
	protected $return_type = 'array';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file ti_designs_designs_ref_model.php */
/* Location: /system/expressionengine/third_party/ti_designs/models/ti_designs_designs_ref_model.php */
