<?php
if (!defined('BASEPATH'))
	die('No direct script access allowed');

if(!class_exists('Ti_designs_model')) 
	require_once PATH_THIRD . "ti_designs/libraries/Ti_designs_model.php";

class Ti_designs_designs_model extends Ti_designs_model
{
	protected $return_type = 'array';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_categories()
	{
		$categories = array();
		$return = array();
		
		ee()->db->group_by('category');
		$results = $this->get_all();
		
		foreach($results as $result)
		{
			$categories = explode(',', $result['category']);
			
			foreach($categories as $category)
			{
				if($category && !in_array(trim($category), $return))
					$return[] = trim($category);
			}
		}
		
		sort($return);
		
		return $return;
	}

	public function save_image_url($design) {

		$data = array ( 'image_url' => $design['image_url']);

		$this->update($design['id'],$data);
	}

}

/* End of file ti_designs_designs_model.php */
/* Location: /system/expressionengine/third_party/ti_designs/models/ti_designs_designs_model.php */
