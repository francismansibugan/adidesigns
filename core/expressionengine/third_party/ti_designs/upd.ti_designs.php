<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require_once PATH_THIRD."ti_designs/config.php";

class Ti_designs_upd
{
	public $version = TI_DESIGNS_VERSION;

	private $module_name = 'Ti_designs';
	private $EE;

	public function __construct()
	{
		// Load forge
		ee()->load->dbforge();
	}

	public function install()
	{
		$this->uninstall();
		
		// Install module
		$mod_data = array(
			'module_name' => $this->module_name,
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		);
		ee()->db->insert('modules', $mod_data);
		
		// Add tables
		$tables = array(
			'ti_designs' => array(
				'rdd_id' => array(
					'type' => 'TEXT'
				),
				'model_number' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'title' => array(
					'type' => 'TEXT'
				),
				'description' => array(
					'type' => 'TEXT'
				),
				'url' => array(
					'type' => 'TEXT'
				),
				'application_description' => array(
					'type' => 'TEXT'
				)
			),
			'ti_designs_designs' => array(
				'id' => array(
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE
				),
				'model_number' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'title' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'description' => array(
					'type' => 'TEXT'
				),
				'url' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'category' => array(
					'type' => 'TEXT'
				),
				'pcb' => array(
					'type' => 'TEXT'
				),
				'schematic' => array(
					'type' => 'TEXT'
				),
				'test_data' => array(
					'type' => 'TEXT'
				),
				'design_file' => array(
					'type' => 'TEXT'
				),
				'bom' => array(
					'type' => 'TEXT'
				),
				'status' => array(
					'type' => 'CHAR',
					'constraint' => 1,
					'default' => 'o'
				),
				'created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
				'updated_at TIMESTAMP'
			),
			'ti_designs_designs_ref' => array(
				'model_number' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'name' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'url' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'releated_devices' => array(
					'type' => 'VARCHAR',
					'constraint' => 255
				),
				'pcb' => array(
					'type' => 'TEXT'
				),
				'schematic' => array(
					'type' => 'TEXT'
				),
				'test_data' => array(
					'type' => 'TEXT'
				),
				'design_file' => array(
					'type' => 'TEXT'
				),
				'bom' => array(
					'type' => 'TEXT'
				)
			)
		);
		
		foreach($tables as $table => $fields)
		{
			ee()->dbforge->add_field($fields);
			
			if($table != 'ti_designs')
			{
				ee()->dbforge->add_key('model_number', TRUE);
			}
			
			if($table == 'ti_designs_designs')
			{
				ee()->dbforge->add_key('id');
				ee()->dbforge->add_key('status');
				ee()->dbforge->add_key('created_at');
				ee()->dbforge->add_key('updated_at');
			}

			ee()->dbforge->create_table($table, TRUE);
		}
		
		return TRUE;
	}

	public function _register_action($method)
	{
		ee()->db->where('class', $this->module_name);
		ee()->db->where('method', $method);
		if (ee()->db->count_all_results('actions') == 0)
		{
			ee()->db->insert('actions', array(
				'class' => $this->module_name,
				'method' => $method
			));
		}
	}

	public function uninstall()
	{
		// Module
		ee()->db->where('module_name', $this->module_name);
		ee()->db->delete('modules');
		
		// Actions
		ee()->db->where('class', $this->module_name);
		ee()->db->delete('actions');
		
		// Tables
		ee()->dbforge->drop_table('ti_designs');
		ee()->dbforge->drop_table('ti_designs_designs');
		ee()->dbforge->drop_table('ti_designs_designs_ref');
		
		//  Config items
		ee()->config->_update_config(
			array(),
			array(
				'ti_designs_url_code' => ''
			)
		);

		return TRUE;
	}

	public function update($current = '')
	{
		if ($current == $this->version)
		{
			// No updates
			return FALSE;
		}

		return TRUE;
	}

}

/* End of file upd.ti_designs.php */
/* Location: /system/expressionengine/third_party/ti_designs/upd.ti_designs.php */