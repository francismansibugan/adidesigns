<div class="pageContents">
	<?=form_open(TI_DESIGNS_URL . AMP . 'method=save_design', 'id=edit_form') ?>
	<?=form_hidden('design_id', $design['id']) ?>
	
	<p>
		<?=form_label(lang('ti_designs_model_number'), 'model_number') ?>
		<?=form_input(array(
				'id' => 'model_number',
				'name' => 'model_number',
				'class' => 'fullfield',
				'value' => set_value('model_number', $design['model_number'])
			))
		?>
	</p>
	
	<p>
		<?=form_label(lang('ti_designs_title'), 'title') ?>
		<?=form_input(array(
				'id' => 'title',
				'name' => 'title',
				'class' => 'fullfield',
				'value' => set_value('title', $design['title'])
			))
		?>
	</p>
	
	<p>
		<?=form_label(lang('ti_designs_description'), 'description') ?>
		<?=form_textarea(array(
				'id' => 'description',
				'name' => 'description',
				'class' => 'fullfield Wysihat-field',
				'rows' => '10',
				'value' => set_value('description', $design['description'])
			))
		?>
	</p>
	
	<p>
		<?=form_label(lang('ti_designs_url'), 'url') ?>
		<?=form_input(array(
				'id' => 'url',
				'name' => 'url',
				'class' => 'fullfield',
				'value' => set_value('url', $design['url'])
			))
		?>
	</p>

	<p>
		<?=form_label(lang('ti_designs_image_url'), 'image_url') ?>
		<?=form_input(array(
			'id' => 'image_url',
			'name' => 'image_url',
			'class' => 'fullfield',
			'value' => set_value('image_url', $design['image_url'])
		))
		?>
	</p>
	
	<p>
		<?=form_label(lang('ti_designs_category'), 'category') ?>
		<?=form_multiselect('category[]',$category_options, $design['category'], 'size="10" class="fullfield"');?>
	</p>
	
	<table id="pcb" width="100%">
		<tr>
			<td colspan="2"><?=form_label(lang('ti_designs_pcb'), 'pcb') ?></td>
		</tr>
		<?php foreach($design['pcb'] as $pcb):?>
		<tr>
			<td style="width:90%">
				<?=form_input(array(
						'name' => 'pcb[]',
						'class' => 'fullfield',
						'value' => set_value('pcb', $pcb)
					))
				?>
			</td>
			<td style="width:8%"><a href="#" class="remove-field"><?=lang('remove')?></a></td>
		</tr>
		<?php endforeach;?>
	</table>
	<?=form_button(array(
			'id' => 'pcb-add',
			'class' => 'submit add-field'
		), lang('ti_designs_add_pcb'))
	?>
	
	<table id="schematic" width="100%">
		<tr>
			<td colspan="2"><?=form_label(lang('ti_designs_schematic'), 'schematic') ?></td>
		</tr>
		<?php foreach($design['schematic'] as $schematic):?>
		<tr>
			<td style="width:90%">
				<?=form_input(array(
						'name' => 'schematic[]',
						'class' => 'fullfield',
						'value' => set_value('schematic', $schematic)
					))
				?>
			</td>
			<td style="width:8%"><a href="#" class="remove-field"><?=lang('remove')?></a></td>
		</tr>
		<?php endforeach;?>
	</table>
	<?=form_button(array(
			'id' => 'schematic-add',
			'class' => 'submit add-field'
		), lang('ti_designs_add_schematic'))
	?>
	
	<table id="test_data" width="100%">
		<tr>
			<td colspan="2"><?=form_label(lang('ti_designs_test_data'), 'test_data') ?></td>
		</tr>
		<?php foreach($design['test_data'] as $test_data):?>
		<tr>
			<td style="width:90%">
				<?=form_input(array(
						'name' => 'test_data[]',
						'class' => 'fullfield',
						'value' => set_value('test_data', $test_data)
					))
				?>
			</td>
			<td style="width:8%"><a href="#" class="remove-field"><?=lang('remove')?></a></td>
		</tr>
		<?php endforeach;?>
	</table>
	<?=form_button(array(
			'id' => 'test_data-add',
			'class' => 'submit add-field'
		), lang('ti_designs_add_test_data'))
	?>
	
	<table id="design_file" width="100%">
		<tr>
			<td colspan="2"><?=form_label(lang('ti_designs_design_file'), 'design_file') ?></td>
		</tr>
		<?php foreach($design['design_file'] as $design_file):?>
		<tr>
			<td style="width:90%">
				<?=form_input(array(
						'name' => 'design_file[]',
						'class' => 'fullfield',
						'value' => set_value('design_file', $design_file)
					))
				?>
			</td>
			<td style="width:8%"><a href="#" class="remove-field"><?=lang('remove')?></a></td>
		</tr>
		<?php endforeach;?>
	</table>
	<?=form_button(array(
			'id' => 'design_file-add',
			'class' => 'submit add-field'
		), lang('ti_designs_add_design_file'))
	?>
	
	<table id="bom" width="100%">
		<tr>
			<td colspan="2"><?=form_label(lang('ti_designs_bom'), 'bom') ?></td>
		</tr>
		<?php foreach($design['bom'] as $bom):?>
		<tr>
			<td style="width:90%">
				<?=form_input(array(
						'name' => 'bom[]',
						'class' => 'fullfield',
						'value' => set_value('bom', $bom)
					))
				?>
			</td>
			<td style="width:8%"><a href="#" class="remove-field"><?=lang('remove')?></a></td>
		</tr>
		<?php endforeach;?>
	</table>
	<?=form_button(array(
			'id' => 'bom-add',
			'class' => 'submit add-field'
		), lang('ti_designs_add_bom'))
	?>
	
	<p style="margin-top:10px;">
		<?=form_label(lang('ti_designs_status'), 'status') ?>
		<?=form_dropdown('status', $status_options, $design['status'])
		?>
	</p>
	<div class="error" style="color: red;text-align: center"></div>
	<p style="margin-top:20px;">
	<?=form_submit(array(
			'name' => 'submit',
			'value' => lang('submit'),
			'class' => 'submit'
		));
	?>
	 &nbsp;<?=lang('or')?>
	<a id="cancel" href="<?=TI_DESIGNS_URL.AMP?>"><?=lang('cancel')?></a>
	</p>
	
	<?=form_close() ?>
</div>