<div style="width: 49%; float: left; margin-left: 1%">
		<h3><?=lang('ti_designs_import')?></h3>
		<?=form_open_multipart(TI_DESIGNS_PATH.AMP.'method=import_csv')?>
		<table border="0" cellspacing="0" cellpadding="5" style="margin:25px">
			<?php if ($upload_errors): ?><tr><td></td><td style="color:#FF0000"><?=lang('ti_designs_upload_error')?></td></tr><?php endif; ?>
			<tr>
				<td><strong>File:</strong></td><td>
				<input type="file" name="file" value="" /></td>
			</tr>
			<tr>
				<td><strong>Type:</strong></td>
				<td>
					<?=form_dropdown('csv_type', array(
						'reference' => 'Designs with PDF references',
						'designs' => 'Designs with description and categories'
						))
					?>
				</td>
			</tr>
			<tr><td></td><td><?=form_submit('', lang('ti_designs_upload'), 'class="submit"')?></td></tr>
		</table>
		<?=form_close()?>
</div>
<div style="width: 49%; float: right; margin-right: 1%">
		<?php
		echo form_open_multipart(TI_DESIGNS_PATH.AMP.'method=update_preferences');
		ee()->table->set_heading(
			lang('ti_designs_preference'),
			lang('ti_designs_setting')
		);
		
		ee()->table->add_row(
			'<strong>'.lang('ti_designs_prefix').'</strong><code style="float:right">?</code>',
			array(
				'style'	=> 'width:50%',
				'data'	=> form_input(array(
					'id' => 'prefix',
					'name' => 'prefix',
					'class' => 'fullfield',
					'value' => set_value('prefix', $prefix)
				))
			)
		);

		ee()->table->add_row(
			'<strong>'.lang('ti_designs_url_code').'</strong><code style="float:right">=</code>',
			array(
				'style'	=> 'width:50%',
				'data'	=> form_input(array(
					'id' => 'url_code',
					'name' => 'url_code',
					'class' => 'fullfield',
					'value' => set_value('url_code', $url_code)
				))
			)
		);
		
		echo ee()->table->generate();
		
		if($prefs_errors)
			echo '<div style="color:#FF0000">' . lang('ti_designs_prefs_error') . '</div>';
		
		echo form_submit(array(
			'name' => 'submit',
			'value' => lang('submit'),
			'class' => 'submit'
		));
		echo form_close();
		?>
</div>
</div></div>
<div class="contents">
	<div class="heading"><h2><?=lang('ti_designs_designs')?></h2></div>
	<div class="pageContents">
	<?=form_open(TI_DESIGNS_URL, 'id="ti_designs_filter"')?>
	<fieldset class="shun">
		<legend><?=lang('ti_designs_filter')?></legend>
		<div class="group">
			<label for="keywords" class="js_hide"><?=lang('keywords')?> </label><?=form_input('keywords', $keywords, 'class="field shun" placeholder="'.lang('keywords').'"')?><br />
			<?=form_submit('submit', lang('search'), 'class="submit" id="search_button"').NBS.NBS?>
			<img src="<?=$cp_theme_url?>images/indicator.gif" class="searchIndicator" alt="Edit Search Indicator" style="margin-bottom: -5px; visibility: hidden;" width="16" height="16" />
			<div style="float:right;display:block;margin-bottom:15px;">
				<?='<a class="ti_designs_create submit" onmouseover="this.style.textDecoration = \'none\'" href="' . TI_DESIGNS_URL . AMP . 'design_id=0&method=edit_design">' . lang('ti_designs_create') . '</a>' ?>
			</div>
		</div>
	</fieldset>
	<?=form_close()?>
	<?=form_open(TI_DESIGNS_PATH.AMP.'method=modify_designs', array('name' => 'target', 'id' => 'target'))?>
	<?=$table_html?>
	<?=$pagination_html?>
	<div class="tableSubmit">
		<?=form_submit('submit', lang('submit'), 'class="submit"').NBS.NBS?>
		<?=form_dropdown('action', $form_options, '', 'id="ti_designs_action"').NBS.NBS?>
	</div>
	<?=form_close()?>
	</div>
</div>