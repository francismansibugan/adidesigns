<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ti_designs_lib
{
	public $code = '';

	public function __construct()
	{
		// load library
		//ee()->load->library();
		
		// load models
		ee()->load->model('ti_designs_designs_model', 'designs');

		// load language
		ee()->lang->loadfile('ti_designs');
	}
	
	public function process_csv($file, $type)
	{
		if($type == 'designs')
		{
			ee()->db->truncate('exp_ti_designs');
			ee()->db->query("LOAD DATA LOCAL INFILE '$file' 
				INTO TABLE `exp_ti_designs` 
				FIELDS TERMINATED BY ',' 
				OPTIONALLY ENCLOSED BY '\"'
				LINES TERMINATED BY '\n'
				IGNORE 1 LINES
				(@dummy,rdd_id,model_number,title,description,url,@dummy,application_description,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy);"
			);
		}
		else if ($type == 'reference')
		{
			ee()->db->truncate('exp_ti_designs_designs_ref');
			ee()->db->query("LOAD DATA LOCAL INFILE '$file' 
				INTO TABLE `exp_ti_designs_designs_ref` 
				FIELDS TERMINATED BY ',' 
				OPTIONALLY ENCLOSED BY '\"'
				LINES TERMINATED BY '\n'
				IGNORE 1 LINES;"
			);
		}
		
		unlink($file);
		
		$this->join_tables();

		return TRUE;
	}

	public function join_tables()
	{
//		ee()->db->query("INSERT INTO `exp_ti_designs_designs`
//						(id,model_number,title,description,url,category,pcb,schematic,test_data,design_file,bom)
//						SELECT `exp_ti_designs`.`rdd_id`,
//						`exp_ti_designs_designs_ref`.`model_number`,
//						`exp_ti_designs_designs_ref`.`name`,
//						`exp_ti_designs`.`description`,
//						`exp_ti_designs_designs_ref`.`url`,
//						`exp_ti_designs`.`application_description`,
//						`exp_ti_designs_designs_ref`.`pcb`,
//						`exp_ti_designs_designs_ref`.`schematic`,
//						`exp_ti_designs_designs_ref`.`test_data`,
//						`exp_ti_designs_designs_ref`.`design_file`,
//						`exp_ti_designs_designs_ref`.`bom`
//						FROM `exp_ti_designs`
//						RIGHT JOIN `exp_ti_designs_designs_ref` ON `exp_ti_designs`.`model_number` = `exp_ti_designs_designs_ref`.`model_number`
//						ON DUPLICATE KEY UPDATE `category` = IF(exp_ti_designs_designs.category <> '', CONCAT(exp_ti_designs_designs.category, ', ',VALUES(`category`)), ''),
//						`description` = IF(exp_ti_designs_designs.description <> '',exp_ti_designs_designs.description,VALUES(`description`))");
        $new_designs = ee()->db->query("
            SELECT `exp_ti_designs_designs_ref`.`model_number`,
                `exp_ti_designs_designs_ref`.`name` AS title,
                `exp_ti_designs`.`description`,
                `exp_ti_designs_designs_ref`.url,
                GROUP_CONCAT(CONCAT(`exp_ti_designs`.`application_description`)) AS category,
                `exp_ti_designs_designs_ref`.`pcb`,
                `exp_ti_designs_designs_ref`.`schematic`,
                `exp_ti_designs_designs_ref`.`test_data`,
                `exp_ti_designs_designs_ref`.`design_file`,
                `exp_ti_designs_designs_ref`.`bom`
            FROM exp_ti_designs_designs_ref
            LEFT JOIN exp_ti_designs ON `exp_ti_designs`.`model_number` = `exp_ti_designs_designs_ref`.`model_number`
            GROUP BY exp_ti_designs_designs_ref.model_number
        ")->result_array();

        foreach($new_designs as $design)
        {
            $model_number = $design['model_number'];

            if(!$model_number)
                continue;

            $dupe = ee()->designs->get_by('model_number', $model_number);

            if ($dupe == array())
            {
                ee()->designs->insert($design);
            } else
            {
                $dupe = array_merge(array_filter($design, 'strval'), array_filter($dupe, 'strval'));

                ee()->designs->update($dupe['id'], $dupe);
            }
        }
						
		$this->update_urls();
	}
	
	public function update_urls($url_code = '', $prefix = '', $old_prefix = '')
	{
		if(empty($url_code) OR empty($prefix) OR empty($old_prefix))
		{
			$url_code = ee()->config->item('ti_designs_url_code');
			$prefix = $old_prefix = ee()->config->item('ti_designs_prefix');
		}

		if (!empty($url_code))
		{
			$designs =  ee()->designs->get_all();

			foreach ($designs as $design)
			{
				$columns = array(
					'url',
					'pcb',
					'schematic',
					'test_data',
					'design_file',
					'bom'
				);
				$data = array();

				foreach ($columns as $column)
				{					
					$data[$column] = implode(',', $this->_add_code(explode(',', $design[$column]), $url_code, $old_prefix));
				}
				
				ee()->designs->update($design['id'], $data);
			}
		}
	}
	
	public function _add_code($urls, $code, $old_prefix)
	{
		if(is_array($urls))
		{
			$arr = array_map(function($value) use ($code, $old_prefix)
			{
				if(empty(trim($value)))
					return;

				return set_query_string($value, $old_prefix, $code);
			}, $urls);

			return $arr;
		}

		return set_query_string($urls, $old_prefix, $code);
	}

}

/* End of file ti_designs_lib.php */
/* Location:
 * /system/expressionengine/third_party/ti_designs/libraries/ti_designs_lib.php
 * */
