<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 =====================================================
 ExpressionEngine - by EEWeb Dev Team
 -----------------------------------------------------
 http://www.innovuze.com/
 =====================================================
 This plugin was created by EEWeb Dev Team
 -  contact@innovuze.com
 -  http://www.innovuze.com/
 =====================================================
 File: pi.get_between.php
 -----------------------------------------------------
 Purpose: Get Between plugin
 =====================================================

 */

$plugin_info = array('pi_name' => 'Get Between', 'pi_version' => '2.0', 'pi_author' => 'EEWeb Dev Team', 'pi_author_url' => 'http://innovuze.com', 'pi_description' => 'Simple CURL', 'pi_usage' => Get_between::usage());

class Get_between
{

    public $return_data;
    public $url;

    /**
     * Constructor
     */
    public function __construct()
    {
        ee()->load->library('web_query_service_lib');
    }

    /**
     * @return bool
     */
    public function site()
    {
        $url = ee()->TMPL->fetch_param('url');
        $selector = ee()->TMPL->fetch_param('selector');
        $attr = ee()->TMPL->fetch_param('attr');

        if ($url == '') return false;

        $this->url = $url;

        $content = ee()->web_query_service_lib->get($url, $selector, $attr);

        foreach ($content->find('a[href^=/]') as $a)
        {
            $a->href = preg_replace('/^\//', $url . '/', $a->href);
        }

        return $content;
    }

    public function find()
    {
        $url = ee()->TMPL->fetch_param('url');
        $selector = ee()->TMPL->fetch_param('selector');

        if ($selector == '' || $url == '') return false;

        $content = ee()->web_query_service_lib->find($selector);

        foreach ($content->find('a[href^=/]') as $a)
        {
            $a->href = preg_replace('/^\//', $url . '/', $a->href);
        }

        return $content;
    }

    // ----------------------------------------------------------------

    /**
     * Plugin Usage
     */
    public static function usage()
    {
    }

}

// END CLASS

/* End of file pi.get_between.php */
/* Location: ./system/expressionengine/plugins/pi.get_between.php */