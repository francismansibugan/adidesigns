<?php
if (!defined('BASEPATH'))
    die('No direct script access allowed');

if(!class_exists('simple_html_dom'))
    require_once('simple_html_dom.php');

class Web_query_service_lib
{

    protected $client;
    protected $crawler;
    protected $url;
    protected $attributes;

    /**
     * Web_query_service constructor.
     */
    function __construct()
    {
        ee()->load->library(array('guzzle'));

        $this->client = new GuzzleHttp\Client();
    }

    /**
     * @param      $url
     * @param null $selector
     * @param bool $attr
     * @return mixed
     */
    public function get($url, $selector = null, $attr = false)
    {
        if ($selector) {
            // load from cache
            $cache_key = md5($url . $selector);

            if ($this->has($cache_key)) {
                return ee()->cache->get($cache_key);
            }
        }

        $this->request('GET', $url);

        if ($selector) {
            return $this->find($selector, $attr);
        }
    }

    /**
     * @param        $selector
     * @param bool   $attr
     * @param string $method
     * @return mixed
     */
    public function find($selector, $attr = false, $method = 'GET')
    {
        // load from cache
        $cache_key = md5($selector . $this->url);

        if ($this->has($cache_key)) {
            return ee()->cache->get($cache_key);
        }

        // attempt to re-initialise request
        if ( ! $this->html) {
            $this->request($method, $this->url);
        }

        $selector = $attr ? $selector . '[' . $attr . ']' : $selector;

        $html = $this->html->find($selector, 0);

        // store
        ee()->cache->save('/page_cache/' . $cache_key, $html);

        return $html;
    }

    /**
     * @param $selector
     * @return mixed
     */
    protected function has($selector)
    {
        return ee()->cache->get($selector);
    }

    /**
     * @param $method
     * @param $url
     * @return $this
     * @throws Exception
     */
    protected function request($method, $url)
    {
        $this->url = $url;
        $response = $this->client->request($method, $this->url);

        // has errors
        $code = $response->getStatusCode();
        if ($code >= 400) {
            throw new Exception('Error ' + $code);
        }

        $this->html = str_get_html($response->getBody()->getContents());

        return $this;
    }

    /**
     * @param       $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, $arguments = [])
    {
        if (method_exists($this, $method)) {
            return call_user_func_array([$this, $method], $arguments);
        }

        return call_user_func_array([$this->attributes, $method], $arguments);
    }
}