<?php

$plugin_info       = array(
	'pi_name'        => 'Redirect 301',
	'pi_version'     => '1.0',
	'pi_author'      => 'April Talampas',
	'pi_author_url'  => '',
	'pi_description' => 'Redirect multiple tagged url to just the first tag.',
	'pi_usage'       => Redirect_301::usage()
	);

class Redirect_301 {
	function Redirect_301() {
		global $TMPL;

		$this->return_data = $this->_move();
	}

	function _move() {
		global $PREFS, $IN;
		$segment_1 = $IN->SEGS[1];

		$lastChar = substr($_SERVER['REQUEST_URI'], -1);
		$curUrl = ($lastChar == '/') ? substr($_SERVER['REQUEST_URI'], 1, -1) : substr($_SERVER['REQUEST_URI'], 1);

		$urlArr = explode("/", $curUrl);
		$count = count($urlArr);

		$urlInfo = array();
		$nth_segment = 0;

		for ($i=0; $i < count($urlArr); $i++) { 
			$urlInfo['segment_'.($i+1)] = $urlArr[$i];
			if(preg_match('/\+/', $urlArr[$i])) $nth_segment = $i+1;
		}

		if(!$nth_segment) return;

		if($nth_segment && ($segment_1 != 'search') ) {

			$site_url = $PREFS->ini('site_url');

			$partsArr = explode("+", $urlInfo["segment_".$nth_segment]);

			if(count($partsArr) > 1) {
				$cntPcs = count($partsArr);
				$segments = "";

				for ($i=0; $i < $nth_segment; $i++) { 
					if($i) $segments .= $urlInfo["segment_".$i]. "/";
				}

				$permanentLink = $site_url.$segments.$partsArr[0];

				header("HTTP/1.1 301 Moved Permanently");
				header("Location: ".$permanentLink);
				exit;
			}
		}
	}

	static function usage()
	{
	ob_start(); 
	?>
	
	Usage:

	Put this {exp:redirect_301} tag into in /head or /community_header template.

	Note: This EE tag only applies to eeweb site.
	
	<?php
	$buffer = ob_get_contents();
	
	ob_end_clean(); 
	
	return $buffer;
	}
}
