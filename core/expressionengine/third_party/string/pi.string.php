<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
	'pi_name'		=> 'String',
	'pi_version'	=> '1.2.0',
	'pi_author'		=> 'Ty Wangsness',
	'pi_author_url'	=> '',
	'pi_description'=> 'Allows string variables to be manipulated and printed in ways that are inconvenient with template tags alone.',
	'pi_usage'		=> String::usage()
);


class String {

	public $return_data;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();

		if(!isset(ee()->session->cache['string'])) {
			ee()->session->cache['string'] = array();
		}
		
		if(ee()->TMPL->fetch_param('name') && isset(ee()->session->cache['string'][ee()->TMPL->fetch_param('name')])) {
			$this->return_data = ee()->session->cache['string'][ee()->TMPL->fetch_param('name')];
		}
	}

	public function set() {
		// $this->EE =& get_instance();
		if(($data = $this->_get_params()) === false) {
			return;
		}
		
		ee()->session->cache['string'][$data['name']] = $data['text'];
	}

	public function clear() {
		// $this->EE =& get_instance();
		if(ee()->TMPL->fetch_param('name') && isset(ee()->session->cache['string'][ee()->TMPL->fetch_param('name')])) {
			unset(ee()->session->cache['string'][ee()->TMPL->fetch_param('name')]);
		}
	}

	public function append() {
		// $this->EE =& get_instance();
		if(($data = $this->_get_params()) === false) {
			return;
		}
		
		if(!isset(ee()->session->cache['string'][$data['name']])) {
			ee()->session->cache['string'][$data['name']] = $data['text'];
		} else {
			$str = "{$data['glue']}{$data['text']}";
			if($data['sep'] != "") {
				$str = "{$data['glue']}{$data['sep']}" . $str;
			}
			ee()->session->cache['string'][$data['name']] = ee()->session->cache['string'][$data['name']] . $str;
		}
	}

	public function prepend() {
		// $this->EE =& get_instance();
		if(($data = $this->_get_params()) === false) {
			return;
		}
		
		if(!isset(ee()->session->cache['string'][$data['name']])) {
			ee()->session->cache['string'][$data['name']] = $data['text'];
		} else {
			$str = "{$data['text']}{$data['glue']}";
			if($data['sep'] != "") {
				$str .= "{$data['sep']}{$data['glue']}";
			}
			ee()->session->cache['string'][$data['name']] = $str . ee()->session->cache['string'][$data['name']];
		}
	}

	public function output() {
		// $this->EE =& get_instance();
		$name = ee()->TMPL->fetch_param('name');
		$value = "";
		
		if($name !== false && isset(ee()->session->cache['string'][$name])) {
			$value = trim(ee()->session->cache['string'][$name]);
		}
		
		if(trim(ee()->TMPL->tagdata) == "") {
			return $value;
		} else {
			$vars = ee()->session->cache['string'];
			$vars['string'] = $value;
			$tagdata = ee()->functions->prep_conditionals(ee()->TMPL->tagdata, $vars);
			foreach($vars as $k=>$v) {
				$tagdata = ee()->TMPL->swap_var_single($k, $v, $tagdata);
			}
			return $tagdata;
		}
	}

	public function _get_params() {
		// $this->EE =& get_instance();
		$ret = array();
		$ret['text']	= false;
		$ret['name']	= ee()->TMPL->fetch_param('name')		? ee()->TMPL->fetch_param('name') : false;
		$ret['sep']		= ee()->TMPL->fetch_param('separator')	? ee()->TMPL->fetch_param('separator') : "";
		$ret['glue']	= ee()->TMPL->fetch_param('glue')		? ee()->TMPL->fetch_param('glue') : "SPACE";
		
		if(ee()->TMPL->tagdata) {
			$ret['text'] = trim(ee()->TMPL->tagdata);
		} else if(ee()->TMPL->fetch_param('text')) {
			$ret['text'] = trim(ee()->TMPL->fetch_param('text'));
		} else if(ee()->TMPL->fetch_param('default')) {
			$ret['text'] = trim(ee()->TMPL->fetch_param('default'));
		}
		
		if(!$ret['name']) {
			return false;
		}
		
		switch($ret['sep']) {
			case "SPACE"   : $ret['sep'] = " "; break;
			case "TAB"	   : $ret['sep'] = "\t"; break;
			case "NEWLINE" : $ret['sep'] = "\n"; break;
			case "NONE"	   : $ret['sep'] = ""; break;
		}
		
		switch($ret['glue']) {
			case "SPACE"   : $ret['glue'] = " "; break;
			case "TAB"	   : $ret['glue'] = "\t"; break;
			case "NEWLINE" : $ret['glue'] = "\n"; break;
			case "NONE"	   : $ret['glue'] = ""; break;
		}
		
		return $ret;
	}

	// ----------------------------------------------------------------

	/**
	 * Plugin Usage
	 */
	public static function usage()
	{
		ob_start();
?>
See http://www.emarketsouth.com/add-ons/string-plugin/ for documentation.
<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}
}
// END CLASS

/* End of file pi.string.php */
/* Location: ./system/expressionengine/plugins/pi.string.php */

?>