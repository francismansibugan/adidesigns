<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * EEWeb Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EEWeb Dev Team
 */

// ------------------------------------------------------------------------

/**
 * Print and Die
 *
 * It just Prints and Dies
 *
 * @access	public
 * @param	mixed
 * @return	void
 */
if ( ! function_exists('print_d'))
{
	function print_d($mixed='here')
	{
		print '<pre>';
		print_r($mixed);
		die();
	}
}

// ------------------------------------------------------------------------

/**
 * UCLower
 *
 * Capitalize first words
 *
 * @access	public
 * @param	string
 * @return	string
 */
if (!function_exists('uclower'))
{
    function uclower($str)
    {
        return ucwords(strtolower($str));
    }

}

// ------------------------------------------------------------------------

/**
 * Strip path
 *
 * Strip path
 *
 * @access	public
 * @param	string
 * @return	string
 */
if (!function_exists('strip_path'))
{
    function strip_path($str)
    {
    	$url = "";
		
    	$parts = parse_url($str);
		
		$url = ($parts['path'] != '/') ? str_replace($parts['path'], '/', $str) : $str;
		
        return $url;
    }

}

// ------------------------------------------------------------------------

/**
 * Get between String
 *
 * Search from a given string
 *
 * @access	public
 * @param	array
 * @param	string
 * @return	string
 */
if (!function_exists('get_between_string'))
{
    function get_between_string($content, $start, $end)
    {
        $r = explode($start, $content);

        if (isset($r[1]))
        { 
            $r = explode($end, $r[1]);
            return $r[0];
        }
        return '';
    }
 
}

// ------------------------------------------------------------------------

/**
 * Get Search Columns Query
 *
 * Search strings from multiple columns
 *
 * @access	public
 * @param	array
 * @param	string
 * @return	string
 */
if (!function_exists('get_sc'))
{
    function get_sc($fields, $query)
    {
        return "(" . implode(" LIKE '%$query%'  OR ", $fields) . " LIKE '%$query%' )";
    }
 
}

// ------------------------------------------------------------------------

/**
 * Clean
 *
 * Remove special characters from a string and convert to short names
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if (!function_exists('clean_string'))
{
    function clean_string($string, $separator = '_')
    {
	   $string = str_replace(' ', $separator, $string);
	
	   return preg_replace('/[^A-Za-z0-9\_]/', '', $string);
	}
 
}

// ------------------------------------------------------------------------

/**
 * Read CSV
 *
 * Read a CSV file and return contents as Array
 *
 * @access	public
 * @param	string
 * @return	array
 */
if (!function_exists('read_csv'))
{
    function read_csv($file)
    {
        $data = array();
        if (($handle = fopen($file, "r")) !== FALSE)
        {
            while (($raw = fgetcsv($handle, 1000, ",")) !== FALSE)
            {
                $data[] = $raw;
            }

            fclose($handle);
        }

        return $data;
    }

}

// ------------------------------------------------------------------------

/**
 * Output CSV
 *
 * Output and Array to a CSV file
 *
 * @access	public
 * @param	array
 * @param	string
 * @return	void
 */
if (!function_exists('out_csv'))
{
    function out_csv($output, $filename)
    {
         foreach ($output as $value)
        {
            fputcsv($outstream, $value, ',', '"');
        }
        fclose($outstream);
        $content = ob_get_contents();
        ob_end_clean();
		
		// force download to csv
		ee()->load->helper('download');
		force_download('module_name_export_' . time() . '.csv', $content);
    }

}

// ------------------------------------------------------------------------

/**
 * Array Key Insensitive Exists
 *
 * Case sensitive version of array_key_exists() using preg_match()
 *
 * @param string
 * @param array
 * @return bool
 */
if ( ! function_exists('array_ikey_exists')) {
    function array_ikey_exists($key, $arr)
    {
        if (preg_match("/" . $key . "/i", join(",", array_keys($arr)))) return true; else
            return false;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_value_by_ikey')) {
    function get_value_by_ikey($key, $arr)
    {
        foreach ($arr as $arr_key => $value) {
            if (strtoupper($key) == strtoupper($arr_key)) {
                return $value;
            }
        }

        return false;
    }
}

// ------------------------------------------------------------------------

/**
 * Append additional query parameters
 *
 * Search from a given string
 *
 * @param string
 * @param string
 * @param string
 * @return string
 */
if ( ! function_exists('set_query_string')) {
    function set_query_string($url, $key, $val)
    {
        $queries = array();
        $parsed_url = parse_url($url);
        $queries[$key] = $val;

        if (isset($parsed_url['query'])) parse_str($parsed_url['query'], $parsed_url['query']); else $parsed_url['query'] = array($key => $val);
        
        foreach ($parsed_url['query'] as $query_key => $query) {
            if (strtoupper($query_key) == strtoupper($key)) {
                $queries[$key] = $val; 
            } else {
                $queries[$query_key] = $query;
            }
        }

        $scheme = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $path = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        $path = count($parsed_url['query']) > 0 ? $path . '?' : $path;

        return $scheme . $host . $path . http_build_query($queries) . $fragment;
    }
}